#Compiler

##Summary
This project Compiles mini-Pascal code into mips assembly. Java will be the language used to accomplish this. After the file goes through this compiler, it will output a MIPS assembly file (.asm). The assembly file can be ran using QTSPIM.

This project will have several different modules. It will consist of a Scanner, Parser, SyntaxTree, Semantic Analyzer, and Code Generation.

###Scanner
The Scanner uses JFlex as the back bone in order to create a Scanner. JFlex takes in a particular language and grammar defended by the user and converts it into java source code. The Scanner is designed to scan Mini-pascal. It'll take in a text file and output tokens to be used later in the compiler.

###Parser
The Parser will recursively run though the outputs of the scanner inorder to make sure the proper syntax is used. it will also though errors if the proper syntax is not used. It also builds a Syntax Tree that models the program.

###SyntaxTree
The Syntax Tree will be generated from the Parser. It's a tree that holds the Syntax of the Program. It will be used later by the Semantic Analysis and Code Generation. This package contains 20 different classes to model all of the possibilities for a successful pascal program.

###Semantic Analyzer
The Semantic Analyzer will traverse the Syntax Tree is correct. It will check if variables have been declared before they are used, the return types match, and the proper number of arguments are being passed into the function or procedure.
 
It also has a code folding feature. This feature will traverse the tree the same time as the Semantic Analyzer and fold an expression. This is done in order to save on assembly code needing to be generated.

###Code Generation
The Code Generation module takes in a Syntax Tree. It will traverse the tree generating code for each operation. It can handle function calls, procedure calls, and even recursive calls

##How to run
	1. Clone or download the source code and compile it.
	2. Pass in a .pas file. It'll output a .asm file.
	3. Use QTSPIM to run that .asm file.