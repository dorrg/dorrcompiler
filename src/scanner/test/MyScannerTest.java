package scanner.test;

import scanner.MyScanner;
import scanner.Token;
import scanner.TokenType;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.*;

/**
 * Created by Greg on 1/19/2017.
 */
public class MyScannerTest {

    FileInputStream fis = null;

    @org.junit.Before
    public void setUp() throws Exception {
        fis = new FileInputStream("C:\\Users\\Greg\\IdeaProjects\\compiler\\src\\scanner\\simplest.pas");
    }

    @org.junit.Test
    public void testYytext()throws Exception{
        InputStreamReader isr = new InputStreamReader(fis);
        MyScanner scanner = new MyScanner(isr);

        String text = scanner.yytext();
        assertEquals("", text);

        scanner.nextToken();
        text = scanner.yytext();
        assertEquals("program", text);

        scanner.nextToken();
        text = scanner.yytext();
        assertEquals("foo", text);

        scanner.nextToken();
        text = scanner.yytext();
        assertEquals(";", text);

        scanner.nextToken();
        text = scanner.yytext();
        assertEquals("begin", text);

        scanner.nextToken();
        text = scanner.yytext();
        assertEquals("end", text);

        scanner.nextToken();
        text = scanner.yytext();
        assertEquals(".", text);
    }


    @org.junit.Test
    public void testNextToken()throws Exception{
        InputStreamReader isr = new InputStreamReader(fis);
        MyScanner scanner = new MyScanner(isr);

        Token token = scanner.nextToken();
        assertEquals(TokenType.PROGRAM, token.getType());

        token = scanner.nextToken();
        assertEquals(TokenType.ID, token.getType());

        token = scanner.nextToken();
        assertEquals(TokenType.SEMI_COLON, token.getType());

        token = scanner.nextToken();
        assertEquals(TokenType.BEGIN, token.getType());

        token = scanner.nextToken();
        assertEquals(TokenType.END, token.getType());

        token = scanner.nextToken();
        assertEquals(TokenType.PERIOD, token.getType());
    }

}