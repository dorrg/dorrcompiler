package scanner;

import java.util.HashMap;

/**
 * @author Greg
 *
 *	This class is used as the look up table for the keywords.
 *	For the key it uses a string of all the keywords. 
 *	The value is the enumerated type keywords and the keyword.
 */
public class LookUpTable extends HashMap<String, TokenType>{

	//Default constructor
	public LookUpTable(){
		this.put("and", TokenType.AND);
		this.put("array", TokenType.ARRAY);
		this.put("begin", TokenType.BEGIN);
		this.put("div", TokenType.DIV);
		this.put("do", TokenType.DO);
		this.put("else", TokenType.ELSE);
		this.put("end", TokenType.END);
		this.put("function", TokenType.FUNCTION);
		this.put("if", TokenType.IF);
		this.put("integer", TokenType.INTEGER);
		this.put("mod", TokenType.MOD);
		this.put("not", TokenType.NOT);
		this.put("of", TokenType.OF);
		this.put("or", TokenType.OR);
		this.put("procedure", TokenType.PROCEDURE);
		this.put("program", TokenType.PROGRAM);
		this.put("real", TokenType.REAL);
		this.put("then", TokenType.THEN);
		this.put("var", TokenType.VAR);
		this.put("while", TokenType.WHILE);
		this.put("read",TokenType.READ);
		this.put("write",TokenType.WRITE);
		this.put(";", TokenType.SEMI_COLON);
		this.put(",", TokenType.COMMA);
		this.put(".", TokenType.PERIOD);
		this.put(":", TokenType.COLON);
		this.put("[", TokenType.LEFT_SQUARE_BRACKET);
		this.put("]", TokenType.RIGHT_SQUARE_BRACKET);
		this.put("(", TokenType.LEFT_PARENTHESES);
		this.put(")", TokenType.RIGHT_PARENTHESES);
		this.put("+", TokenType.PLUS);
		this.put("-", TokenType.MINUS);
		this.put("=", TokenType.EQUALITY_OPERATOR);
		this.put("<>", TokenType.NOT_EQUAL);
		this.put("<", TokenType.LESS_THAN);
		this.put("<=", TokenType.LESS_THAN_EQUAL_TO);
		this.put(">", TokenType.GREATER_THAN);
		this.put(">=", TokenType.GREATER_THAN_EQUAL_TO);
		this.put("*", TokenType.TIMES);
		this.put("/", TokenType.DIVIDE);
		this.put(":=", TokenType.ASSIGNMENT_OPERATOR);
	}
}
