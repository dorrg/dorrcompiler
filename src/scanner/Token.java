package scanner;

/**
 * The purpose of the Token class is to store items from the Scanner
 * @author Greg
 *
 */
public class Token {
	
	//compiler variables
	private String contents;	//The content
	private TokenType type;		//The data type
	private int lineNumber;     //Line number token is found at



	/**
	 * Constructor that accepts two arguments.
	 * @param input the lexeme coming from the scanner
	 * @param type the type of token it is. based on the keyword enum type
	 */
	public Token(String input, TokenType type) {
		this.contents = input;
		this.type = type;
	}

    /**
     * Constructor that accepts two arguments.
     * @param input the lexeme coming from the scanner
     * @param type the type of token it is. based on the keyword enum type
	 * @param num the line number of the operation
     */
    public Token(String input, TokenType type, int num) {
        this.contents = input;
        this.type = type;
        this.lineNumber=num;
    }

	/**
	 * @return contents returns the string received from the scanner.
	 */
	public String getLexeme() {
		return this.contents;
	}

	/**
	 * @return The token type determined by the look up table class
	 */
	public TokenType getType() {
		return this.type;
	}

    /**
     * @return the line number the token was found in the original document
     */
	public int getLineNumber(){
	    return this.lineNumber;
    }

	@Override
	public String toString() {
		return ("Token: " + this.contents + ", " + this.type);
	}

}
