/**
 *	This is a simple example of a jflex lexer definition
 */
package scanner;
import scanner.LookUpTable;
import scanner.Token;
import scanner.TokenType;
/* Declarations */

%%
%public
%class MyScanner 	/* names produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
%{
	LookUpTable map = new LookUpTable();
	int lineNumber = 1;
%}

/* Patterns */
other			  = .
letter		 	  = [A-Za-z]
digit 		 	  = [0-9]
whitespace   	  = [ \n\t\r]|(([\{])([^\{])*([\}]))
symbol 		 	  = [;,.:\[\]()+-=<>\*/] 
id 		 	 	  = ({letter}+)({letter}|{digit})*
symbols 	 	  = {symbol}|:=|<=|>=|<>
digits 		 	  = ({digit})({digit}*)
optional_fraction = ([.])({digits})
optional_exponent = ([E]([+]|[-])?{digits})
num 			  = {digits}{optional_fraction}?{optional_exponent}?{optional_fraction}?

%%
/* Lexical Rules */
{id}		{
				TokenType key = map.get(yytext());
				if(key == null){
					return (new Token(yytext(), TokenType.ID, lineNumber));
				}
				else{
					return (new Token(yytext(), key, lineNumber));
				}
				
			}
			
{whitespace}	{ 
					/*ignore whitespace*/
					if((yytext().charAt(0)=='{')&&(yytext().charAt(yytext().length()-1)=='}'))
						//ignore comments
					if(yytext().equals("\n"))
					    lineNumber++;
				}

{num}		{
				return new Token(yytext(),TokenType.NUMBER, lineNumber);
			}

{symbols}	{
				TokenType key = map.get(yytext());
				return(new Token(yytext(),key, lineNumber));
			}

{other}		{
				System.out.println("Illegal char: '" + yytext() + "' found at line: " + lineNumber);
			}