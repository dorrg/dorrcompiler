package symboltable;

/**
 * Created by Greg on 2/20/2017.
 */
public enum Kind {
    FUNCTION_NAME, ARRAY_NAME, VARIABLE_NAME, PROGRAM_NAME, PROCEDURE_NAME
}
