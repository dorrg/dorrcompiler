package symboltable;

import scanner.TokenType;
import java.util.HashMap;
import java.util.Stack;

/**
 * The purpose of the SymbolTable is to hold the Variables, Functions, and Procedures of the Compiler.
 * It implements a stack inorder to deal with scope. The SymbolTable will always store the global hash map.
 * @author Greg
 * created on 4/2/2017.
 */
public class SymbolTable {
    private Stack<HashMap<String,Symbol>> stack;    //A stack for scope
    private int size;                               //current size

    /**
     * Default constructor that initializes the hash map
     */
    public SymbolTable(){
       stack=new Stack();
       size = 0;
    }

    /**
     * Pushing a new HashMap onto the stack inorder to be populated with
     * a subprograms variables and arguments
     */
    public void addNewScope(){
            stack.push(new HashMap<String, Symbol>());
            size++;
    }

    /**
     * Pops of the top Hash Map and returns it.
     * It won't let the global hash map be Popped off.
     * @return The local scope from a function or procedure
     */
    public HashMap<String,Symbol> removeScope(){
        if(size > 1) {
            size--;
            return stack.pop();
        }
        return null;
    }


    /**
     * Pushes a local symbol table onto the stack inorder to evaluate the local scoping
     * @param input To be pushed onto the stack
     */
    public void pushSymbolTable(HashMap<String,Symbol> input){
        stack.push(input);
    }

    /**
     * adds an id to the idTable hash map
     * @param id of the token type
     * @param dataType the data type of the symbol
     * @param type the token type of the symbol
     * @param startIndex of an array. zero if not an array
     * @param endIndex of an array. zero if not an array
     * @return true if it's one of the id token types. false if it's not one of the types
     */
    public boolean addID(String id, TokenType dataType, TokenType type, int startIndex, int endIndex){
        HashMap<String,Symbol> idTable = stack.peek();
        //if the id is already in the table
        if(idTable.containsKey(id)){
            return false;
        }

        //switch based on the TokenType
        switch(type){
            case FUNCTION:
                Symbol func = new Symbol(id, dataType, Kind.FUNCTION_NAME);
                idTable.put(id,func);
                return true;
            case VAR:
                Symbol var = new Symbol(id, dataType, Kind.VARIABLE_NAME);
                idTable.put(id,var);
                return true;
            case ARRAY:
                Symbol array = new Symbol(id, dataType, startIndex, endIndex);
                idTable.put(id,array);
                return true;
            case PROGRAM:
                Symbol program = new Symbol(id, Kind.PROGRAM_NAME);
                idTable.put(id,program);
                return true;
            case PROCEDURE:
                Symbol proc = new Symbol(id, dataType, Kind.PROCEDURE_NAME);
                idTable.put(id,proc);
                return true;
            default:
                return false;
        }
    }

    /**
     * For adding functions or procedures before the return type is known.
     * @param id The id to be added to the symbol table
     * @param type The type of the operation
     * @return true if it's a successful add. False if it's already in the table
     */
   public boolean simpleAdd(String id,TokenType type){
       HashMap<String,Symbol> idTable = stack.peek();
       if(idTable.containsKey(id)){
           return false;
       }
       if(type == TokenType.FUNCTION){
           Symbol func = new Symbol(id, Kind.FUNCTION_NAME);
           idTable.put(id,func);
           return true;
       }
       else if(type == TokenType.PROCEDURE){
           Symbol proc = new Symbol(id, Kind.PROCEDURE_NAME);
           idTable.put(id,proc);
           return true;
       }

       else return false;
   }

    /**
     * Adds a return type to a symbol that's already in the table
     * @param id The symbol to have a return type added to
     * @param type The return type to have it set as
     */
   public void addType(String id, TokenType type){
       for(HashMap<String,Symbol> table: stack){
           if(table.containsKey(id)){
               table.get(id).setDataType(type);
               return;
           }
       }
   }

    /**
     * Checks if the id is already in the hash map
     * and is a function name
     * @param name of the string
     * @return true if it's in the hash map. Else it'll return false;
     */
    public boolean isFunctionName(String name){
        for(HashMap<String,Symbol> idTable: stack) {
            //making sure the name is in there
            if ((idTable.get(name) != null)&&(idTable.get(name).getKind() == Kind.FUNCTION_NAME)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the id is already in the hash map
     * and is a variable name
     * @param name of the string
     * @return true if it's in the hash map. Else it'll return false;
     */
    public boolean isVariableName(String name){
        for(HashMap<String,Symbol> idTable: stack){
            //making sure the name is in there
            if ((idTable.get(name) != null)&&(idTable.get(name).getKind() == Kind.VARIABLE_NAME)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the id is already in the hash map
     * and is an array name
     * @param name of the string
     * @return true if it's in the hash map. Else it'll return false;
     */
    public boolean isArrayName(String name){
        for(HashMap<String,Symbol> idTable: stack){
            //making sure the name is in there
            if ((idTable.get(name) != null)&&(idTable.get(name).getKind() == Kind.ARRAY_NAME)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the id is already in the hash map
     * And is a program Name
     * @param name of the string
     * @return true if it's in the hash map. Else it'll return false;
     */
    public boolean isProgramName(String name){
        for(HashMap<String,Symbol> idTable: stack) {
            //making sure the name is in there
            if ((idTable.get(name) != null)&&(idTable.get(name).getKind() == Kind.PROGRAM_NAME)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if the id is already in the hash map
     * and is a procedure name
     * @param name of the string
     * @return true if it's in the hash map. Else it'll return false;
     */
    public boolean isProcedureName(String name){
        for(HashMap<String,Symbol> idTable: stack){
            //making sure the name is in there
            if ((idTable.get(name) != null)&&(idTable.get(name).getKind() == Kind.PROCEDURE_NAME)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the TokenType (Data type) of the Symbol.
     * This could be either Integer, or Real.
     * @param name of the symbol
     * @return The return type of the name being passed in
     */
    public TokenType getReturnType(String name){
        for(HashMap<String,Symbol> idTable: stack) {
            if (idTable.get(name) != null)
                return idTable.get(name).getDataType();
        }
        return null;
    }

    /**
     * Returns the size of an array in the symbol table.
     * @param name of the array needing the size found
     * @return the size of the array. it will return zero if it's not in the table
     */
    public int getArraySize(String name){
        if(isArrayName(name)){
            Symbol symbol = stack.peek().get(name);
            return (symbol.getEndIndex()-symbol.getStartIndex());
        }
        else
            return 0;
    }

    /**
     * Set the Key's local table to the hashmap being passed in
     * @param key of the symbol
     * @param input the HashMap to set as the local table
     */
    public void setLocalTable(String key, HashMap<String,Symbol> input){
        if(stack.peek().containsKey(key)){
            stack.peek().get(key).setLocalTable(input);
        }
        else
            System.err.println("**ERROR: The key is not in the hash map**");
    }

    /**
     * Returns the local table of the key passed into it.
     * If it exists.
     * @param key of the symbol to get the local table from
     * @return the HashMap of the Key's local table
     */
    public HashMap getLocalTable(String key){
        for(HashMap<String,Symbol> map: stack) {
            if (map.containsKey(key)) {
                return map.get(key).getLocalTable();
            }
        }
        return null;

    }


    /**
     * Returns the Memory address of the variableName passed in
     * @param varName The variable to find the Memory Address off
     * @return The memory address of the Variable
     */
    public String getMemoryAddress(String varName) {
        for(HashMap<String,Symbol> idTable: stack) {
            if (idTable.containsKey(varName))
                return idTable.get(varName).getMemoryAddress();
        }
        return null;
    }

    /**
     * Sets the Memory address of the VarName to the input passed in
     * @param varName to have the Memory Address set
     * @param input The Memory Address
     */
    public void setMemoryAddress(String varName, String input){
        for(HashMap<String,Symbol> idTable: stack) {
            if (idTable.get(varName) != null) {
                idTable.get(varName).setMemoryAddress(input);
                return;
            }
        }
    }

    /**
     * returns a string representation of the object
     * @return a string version of the object
     */
    @Override
    public String toString() {
        String text="";
        for(HashMap<String, Symbol> idTable: stack) {
            text= idTable.toString().replaceAll("[=]", " = ").replaceAll("\n,", "\n");
            text+="\n";
        }
        return "SymbolTable{\n" +
                "Global Table = \n" + text +
                '}';
    }

    /**
     * private nested class to give more information about the particular symbol
     * It has fields for the id, data type, token type, start and end index.
     */
    private class Symbol{
        private final String id;                          //The name of the symbol
        private String memoryAddress;                     //Its memory address
        private TokenType dataType;                       //The Data type of the symbol (Integer or Real)
        private final Kind type;                          //The kind of symbol
        private final int startIndex;                     //start index for an array
        private final int endIndex;                       //end index for an array
        private HashMap<String,Symbol> localTable;        //local table used in procedures and functions
        /**
         * Constructor for Program
         * defaults the startIndex and endIndex to zero
         * @param id of the token
         * @param type The token type of the object
         */
        public Symbol(String id, Kind type){
            this.id = id;
            this.dataType = null;
            this.type = type;
            this.startIndex = 0;
            this.endIndex = 0;
            this.localTable=null;
        }

        /**
         * Constructor for variable, Function
         * defaults the startIndex and endIndex to zero
         * @param id of the token
         * @param dataType the data type of id Integer or Real
         * @param type The token type of the object
         */
        public Symbol(String id, TokenType dataType, Kind type){
            this.id = id;
            this.dataType = dataType;
            this.type = type;
            this.startIndex = 0;
            this.endIndex = 0;
            this.localTable=null;
        }


        /**
         * Constructor for if the symbol is an array
         * @param id of the token
         * @param dataType the data type of id Integer or Real
         * @param startIndex start index of an array
         * @param endIndex end index of the array
         */
        public Symbol(String id, TokenType dataType, int startIndex, int endIndex){
            this.id = id;
            this.dataType = dataType;
            this.type = Kind.ARRAY_NAME;
            this.startIndex = startIndex;
            this.endIndex = endIndex;
            this.localTable=null;

        }

        /**
         * gets the id
         * @return a string representation of the ID
         */
        public String getId(){
            return id;
        }

        /**
         * returns the data type of the symbol
         * @return the data type of the symbol
         */
        public TokenType getDataType(){
            return this.dataType;
        }

        /**
         * Returns the particular token type
         * @return the token type of the symbol
         */
        public Kind getKind(){
            return this.type;
        }

        /**
         * returns the start index of an array. If the symbol isn't an array,
         * it'll return zero
         * @return start Index for an array. It'll return zero if it's not an array
         */
        public int getStartIndex(){
            return this.startIndex;
        }

        /**
         * returns the end index of an array. If the symbol isn't an array,
         * it'll return zero
         * @return end Index for an array. It'll return zero if it's not an array
         */
        public int getEndIndex(){
            return this.endIndex;
        }

        /**
         * Sets the local table to the argument passed in
         * @param input Hashmap to set as the local table
         */
        public void setLocalTable(HashMap<String,Symbol> input){
            localTable=input;
        }

        /**
         * Returns the local table
         * @return The local table
         */
        public HashMap getLocalTable(){
            return this.localTable;
        }

        /**
         * Sets the memory address of the local variable
         * @param input the memory address.
         */
        public void setMemoryAddress(String input){
            this.memoryAddress =input;
        }

        /**
         * Returns the memory address of the Symbol
         * @return the memory address of the symbol
         */
        public String getMemoryAddress(){return this.memoryAddress;}

        /**
         * Sets the data type of the Symbol
         * @param dataType to be set as data type
         */
        public void setDataType(TokenType dataType){
            this.dataType=dataType;
        }


        /**
         * @return a string version of the object
         */
        @Override
        public String toString() {
            StringBuilder str = new StringBuilder("( id=");
            str.append(id);
            str.append(", dataType=");
            str.append(dataType);
            str.append(", Type=");
            str.append(type);
            str.append(", startIndex=");
            str.append(startIndex);
            str.append(", endIndex=");
            str.append(endIndex);
            str.append(", MemoryAddress=");
            str.append(memoryAddress);
            if(localTable != null) {
                str.append("\n\t\t\t");
                str.append(id + " local table:\n\t\t\t");
                str.append(localTable.toString().replaceAll("\n","\n\t\t\t").replaceAll("\t,", "\t"));
            }
            str.append(")\t\n");
            return str.toString();
        }
    }
}
