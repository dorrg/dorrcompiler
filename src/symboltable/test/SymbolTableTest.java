package symboltable.test;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import symboltable.SymbolTable;
import scanner.TokenType;
/**
 * Created by Greg on 2/12/2017.
 */
public class SymbolTableTest {



    SymbolTable st1;
    @Before
    public void setup(){
        st1 = new SymbolTable();

        //adding several function id's
        st1.addID("myfunc1", TokenType.REAL, TokenType.FUNCTION, 0, 0);
        st1.addID("myfunc2", TokenType.INTEGER, TokenType.FUNCTION,0,0);
        st1.addID("myfunc3", TokenType.INTEGER, TokenType.FUNCTION, 0, 0);

        //adding variables
        st1.addID("var1", TokenType.REAL, TokenType.VAR,  0, 0);
        st1.addID("var2", TokenType.INTEGER, TokenType.VAR,0,0);
        st1.addID("var3", TokenType.INTEGER, TokenType.VAR, 0, 0);

        //adding array id's
        st1.addID("array1", TokenType.REAL, TokenType.ARRAY, -5, 10);
        st1.addID("array2", TokenType.INTEGER, TokenType.ARRAY,8,12);
        st1.addID("array3", TokenType.INTEGER, TokenType.ARRAY, 9, 1);

        //adding program ids
        st1.addID("program1", TokenType.REAL, TokenType.PROGRAM, 0, 0);
        st1.addID("program2", TokenType.INTEGER, TokenType.PROGRAM,0,0);
        st1.addID("program3", TokenType.INTEGER, TokenType.PROGRAM, 0, 0);

    }


    @Test
    public void addID() throws Exception {
        System.out.println("\n-----------ADDID----------");

        SymbolTable st2 = new SymbolTable();

        //adding several function id's
        //should return true
        String name = "myfunc1";
        boolean result = st2.addID(name, TokenType.REAL, TokenType.FUNCTION,  0, 0);
        assertEquals(true,result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");

        //function #2
        name = "myfunc2";
        result = st2.addID(name, TokenType.INTEGER, TokenType.FUNCTION,0,0);
        assertEquals(true, result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");


        //adding variables
        name = "var1";
        result = st2.addID(name, TokenType.REAL, TokenType.VAR, 0, 0);
        assertEquals(true, result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");

        //variable #2
        name = "var2";
        result = st2.addID(name, TokenType.INTEGER, TokenType.VAR,0,0);
        assertEquals(true, result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");

        //adding array id's
        name = "array1";
        result =st2.addID(name, TokenType.REAL, TokenType.ARRAY, -5, 10);
        assertEquals(true, result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");

        //array #2
        name = "array2";
        result = st2.addID(name, TokenType.INTEGER, TokenType.ARRAY,8,12);
        assertEquals(true, result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");

        //adding program ids
        name = "program1";
        result = st2.addID(name, TokenType.REAL, TokenType.PROGRAM, 0, 0);
        assertEquals(true, result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");

        //program2
        name = "program2";
        result = st2.addID("program2", TokenType.INTEGER, TokenType.PROGRAM,0,0);
        assertEquals(true, result);
        System.out.println("\'" + name + "\' -- successfully added to the SymbolTable!");

        //trying to add a non-id type to the list
        name = "AND";
        result = st2.addID(name, TokenType.INTEGER, TokenType.AND,0,0);
        assertEquals(false, result);
        System.out.println("\'" + name + "\' -- was *NOT* successfully added to the SymbolTable!");

    }

    @Test
    public void isFunctionName() throws Exception {
        System.out.println("\n-----------isFunctionName----------");

        //checking if "myfunc" is in the hash map
        String name = "myfunc1";
        boolean result = st1.isFunctionName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a FUNCTION id in the SymbolTable!");

        //function #2
        name = "myfunc2";
        result = st1.isFunctionName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a FUNCTION id in the SymbolTable!");

        //function #3
        name = "myfunc3";
        result = st1.isFunctionName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a FUNCTION id in the SymbolTable!");

        //checking if var1 is a FUNCTION id
        name = "var1";
        result = st1.isFunctionName(name);
        assertEquals(false,result);
        System.out.println("\'"+name + "\': is **NOT** a FUNCTION id in the SymbolTable!");
    }

    @Test
    public void isVariableName() throws Exception {
        System.out.println("\n-----------isVariableName----------");

        //checking if var1 is in the hashmap
        String name = "var1";
        boolean result = st1.isVariableName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a VAR in the SymbolTable!");

        //Variable #2
        name = "var2";
        result = st1.isVariableName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a VAR id in the SymbolTable!");

        //Variable #2
        name = "var3";
        result = st1.isVariableName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a VAR id in the SymbolTable!");

        //checking if myfunc is a var
        name = "myfunc1";
        result = st1.isVariableName(name);
        assertEquals(false,result);
        System.out.println("\'"+name + "\': is **NOT** a VAR id in the SymbolTable!");
    }

    @Test
    public void isArrayName() throws Exception {
        System.out.println("\n-----------isArrayName----------");

        //checking if "array1" is in the hash map
        String name = "array1";
        boolean result = st1.isArrayName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is an ARRAY id in the SymbolTable!");

        //array #2
        name = "array2";
        result = st1.isArrayName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is an ARRAY id in the SymbolTable!");

        //array #3
        name = "array3";
        result = st1.isArrayName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is an ARRAY id in the SymbolTable!");

        //checking if myfunc is a array
        name = "myfunc1";
        result = st1.isArrayName(name);
        assertEquals(false,result);
        System.out.println("\'"+name + "\': is **NOT** an ARRAY id in the SymbolTable!");
    }

    @Test
    public void isProgramName() throws Exception {
        System.out.println("\n-----------isProgramName----------");

        //checking if "program" is in the hash map
        String name = "program1";
        boolean result = st1.isProgramName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a PROGRAM id in the SymbolTable!");

        //program #2
        name = "program2";
        result = st1.isProgramName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a PROGRAM id in the SymbolTable!");

        //program #2
        name = "program3";
        result = st1.isProgramName(name);
        assertEquals(true,result);
        System.out.println("\'"+name + "\': is a PROGRAM id in the SymbolTable!");

        //checking if myfunc is a program name
        name = "myfunc1";
        result = st1.isProgramName(name);
        assertEquals(false,result);
        System.out.println("\'"+name + "\': is **NOT** a PROGRAM id in the SymbolTable!");
    }

}