package compiler;

import codegenerator.GenerationController;
import parser.Parser;
import semanticanalyzer.SemanticAnalyzer;
import symboltable.SymbolTable;
import syntaxtree.*;
import java.io.File;
import java.io.PrintWriter;

/**
 * This is the main of the compiler. It takes in a file from command line,
 * Scans through it, Parses it, generates a SyntaxTree, does Semantic Analysis,
 * and finally generates mips assembly
 * @author Greg
 * created on 2/18/2017.
 */
public class CompilerMain {

    public static void main(String[] args){

        //making sure the user entered a file
        if( args.length < 1){
            System.err.println("Error: No File was passed in");
            System.exit(1);
        }
        //checking if it's a pascal file
        else if(!args[0].contains(".pas")){
            System.err.println("Error: File is not a .pas");
            System.exit(1);
        }


        //saving the input
        File file = new File(args[0]);

        //Testing to make sure it can be compiled
       if(!file.exists()){
           System.err.println("Error: File does not Exist");
           System.exit(1);
       }
       //Checking if it can be read
       else if(!file.canRead()){
           System.err.println("Error: Can't open, Permission Denied");
           System.exit(1);
       }

        //calling the parser
        Parser p = new Parser(file.getName(),true);
        ProgramNode tree = p.program();
        SymbolTable symbolTable = p.getSymbolTable();

        //calling the semantic analysis
        SemanticAnalyzer sa = new SemanticAnalyzer(tree);
        tree = sa.startCodeFolding();

        //Generating the code
        GenerationController gc = new GenerationController(tree,symbolTable);

        //saving the name of the file without an extensions
        String fileName = file.getName().substring(0,file.getName().indexOf("."));

        //printing out the Tree and Assembly Code
        try {
            //creating new files
            File treeFile = new File(fileName+".tree");
            File assemblyFile = new File(fileName+".asm");

            treeFile.createNewFile();
            assemblyFile.createNewFile();

            //creating new print writers to write to the new files
            PrintWriter writeTree= new PrintWriter(treeFile);
            PrintWriter writeAssembly = new PrintWriter(assemblyFile);

            //writing to the files
            writeAssembly.write(gc.generate());
            writeTree.write(tree.indentedToString(0));

            //closing the files
            writeTree.close();
            writeAssembly.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
