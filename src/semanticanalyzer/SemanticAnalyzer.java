package semanticanalyzer;

import syntaxtree.*;
import scanner.TokenType;
import java.util.ArrayList;

/**
 * The purpose of the Semantic Analyzer is to fold the code. It takes a ProgramNode in for the constructor.
 * It then evaluates expressions to try to make the code more efficient. It outputs a a ProgramNode that's
 * optimized.
 * @author Greg
 * created on 3/30/2017.
 */
public class SemanticAnalyzer {

    /**
     * Root node of the tree to analyze.
     */
    private final ProgramNode root;

    /**
     * Creates a SemanticAnalyzer with the given expression.
     *
     * @param tree An expression tree to analyze.
     */
    public SemanticAnalyzer(ProgramNode tree) {
        this.root = tree;
    }

    /**
     * Starts the Code folding. Outputs the Program node after it
     * has been folded.
     * @return The folded ProgramNode
     */
    public ProgramNode startCodeFolding() {

        //CompoundStatement- from the CompoundStatementNode
        CompoundStatementNode csn = new CompoundStatementNode();
        for (StatementNode state : root.getMain().getStatements()) {
            csn.addStatement(foldStatement(state));
        }

        //Functions - from the subprogramNode
        SubProgramDeclarationsNode spdn = new SubProgramDeclarationsNode();
        for (SubProgramNode sub : root.getFunctions().getProcs()) {
            spdn.addSubProgramDeclaration(foldSubProgram(sub));
        }
        root.setFunctions(spdn);
        root.setMain(csn);

        return root;
    }


    /**
     * Folds subProgram Nodes. Similar to startCode folding
     * @param sub SubprogramNode to be folded
     * @return The folded subProgramNode
     */
    public SubProgramNode foldSubProgram(SubProgramNode sub){

        //fold CompoundStatements
        CompoundStatementNode csn = new CompoundStatementNode();
        for(StatementNode state : sub.getMain().getStatements()){
            csn.addStatement(foldStatement(state));
        }
        //recursively call this function if there's a nested function
       SubProgramDeclarationsNode spdn = new SubProgramDeclarationsNode();
        for(SubProgramNode subprog: sub.getFunctions().getProcs()){
           spdn.addSubProgramDeclaration(foldSubProgram(subprog));
        }

        sub.setMain(csn);
        sub.setFunctions(spdn);

        return sub;


    }
    /**
     * Folds variable nodes. ArrayNodes and FunctionNodes are children of VariableNodes
     * They both have an expression in them. this will fold them if necessary.
     * @param vars to be folded
     * @return the variable node if it was successfully folded.
     */
    public VariableNode foldVars(VariableNode vars){
        switch(vars.getClass().getSimpleName()){
            case"ArrayNode":
                ((ArrayNode)vars).setExpNode(checkExpression(((ArrayNode)vars).getExpNode()));
                break;
            case "FunctionNode":
                ((FunctionNode)vars).setExpNode(checkExpression(((FunctionNode)vars).getExpNode()));
                if(!hasValidArgs(((FunctionNode)vars)))
                    System.err.println("**ERROR- Invalid number of arguments");
                break;
            default:
                break;
        }
        return vars;
    }

    /**
     * Filters though the statements and if they contain an expression, they will call
     * check Expression()
     * @param state Statement to be folded
     * @return the folded statement
     */
    public StatementNode foldStatement(StatementNode state) {
        switch (state.getClass().getSimpleName()) {
            case "AssignmentStatementNode":
                //get the expression, pass it into checkExpression, then assign the result to the new expression
                ((AssignmentStatementNode) state).setExpression(checkExpression(((AssignmentStatementNode) state).getExpression()));
                ((AssignmentStatementNode)state).setLvalue(foldVars(((AssignmentStatementNode)state).getLvalue()));
                break;
            case "CompoundStatementNode":
                for (StatementNode sn : ((CompoundStatementNode) state).getStatements()) {
                    foldStatement(sn);
                }
                break;
            case "IfStatementNode":
                //if TEST
                ((IfStatementNode) state).setTest(checkExpression(((IfStatementNode) state).getTest()));
                //THEN
                foldStatement(((IfStatementNode) state).getThenStatement());
                //ELSE
                foldStatement(((IfStatementNode) state).getElseStatement());
                break;
            case "ProcedureStatementNode":
                ((ProcedureStatementNode) state).setExpNode(checkExpression(((ProcedureStatementNode) state).getExpNode()));
                break;
            case "WhileStatementNode":
                ((WhileStatementNode) state).setTest(checkExpression(((WhileStatementNode) state).getTest()));
                foldStatement(((WhileStatementNode) state).getDoStatement());
                break;
            case "WriteNode":
                checkExpression(((WriteNode)state).getExpNode());
            default:
                break;
        }
        return state;
    }

    /**
     * Takes in an  ExpressionNodes and checks what type it is.
     * It will then call the proper place for it
     * @param exp The expression to be checked
     * @return a folded expressions (if possible)
     */
    public ExpressionNode checkExpression(ExpressionNode exp) {
        if (exp instanceof OperationNode) {
            return codeFolding((OperationNode) exp);
        }
        else if(exp instanceof UnaryOperationNode){
            return unaryFolding((UnaryOperationNode)exp);
        }
        else if(exp instanceof VariableNode){
            return foldVars((VariableNode) exp);
        }
        return exp;
    }

    /**
     * Takes in an array list of ExpressionNodes and checks each one's expressions
     * It will then call the proper place for each one
     * @param exp The list of expressions to be checked
     * @return a new array list full of folded expressions (if possible)
     */
    public ArrayList<ExpressionNode> checkExpression(ArrayList<ExpressionNode> exp) {
        //temp arrayList to hold the values for return
        ArrayList<ExpressionNode> newExp = new ArrayList<>();
        for (ExpressionNode expression : exp) {
            if (expression instanceof OperationNode) {
                newExp.add(codeFolding((OperationNode) expression));
            }
            else if(expression instanceof UnaryOperationNode){
                newExp.add(unaryFolding((UnaryOperationNode)expression));
            }
            else if(expression instanceof VariableNode){
                newExp.add(foldVars((VariableNode) expression));
            }
            else{
                newExp.add(expression);
            }
        }
        return newExp;
    }

    /**
     * folds a unary operation node
     * @param input UnaryOperationNode to be folded
     * @return Folded expression if possible. else returns the input
     */
    public ExpressionNode unaryFolding(UnaryOperationNode input){

        //saving the expression
        ExpressionNode exp = checkExpression(input.getExpression());
        //saving the operation
        TokenType token = input.getOperation();

        //if it's just a value node
        if(exp instanceof ValueNode){
            //its minus
            if(token==TokenType.MINUS) {
                return new ValueNode(("-"+((ValueNode)exp).getAttribute()));
            }
            else if(token==TokenType.PLUS) {
                return new ValueNode((""+((ValueNode)exp).getAttribute()));
            }
            else if(token == TokenType.NOT){
                double value = Double.parseDouble(((ValueNode) exp).getAttribute());
                if(value == 0){
                    return new ValueNode("1");
                }
                else return new ValueNode("0");
            }
            //its positive
            else return new ValueNode((((ValueNode)exp).getAttribute()));
        }
        //if it's not a value, it will return 0 - the expression
        else {
            if(token == TokenType.MINUS){
                OperationNode opNode = new OperationNode(token);
                opNode.setLeft(new ValueNode("0"));
                opNode.setRight(exp);
                return opNode;
            }
            else if(token==TokenType.PLUS) {
                return exp;
            }
            else if(token == TokenType.NOT){
                return input;
            }
            //if it's plus it will remove the plus
            else{
                return exp;
            }

        }
    }


    /**
     * Folds code for the given node.
     * We only fold if both children are value nodes, and the node itself
     * is a PLUS node.
     *
     * @param node The node to check for possible efficiency improvements.
     * @return The folded node or the original node if nothing
     */
    public ExpressionNode codeFolding(OperationNode node) {
        //if they are operation nodes
        if (node.getLeft() instanceof OperationNode) {
            node.setLeft(codeFolding((OperationNode) node.getLeft()));
        }
        if (node.getRight() instanceof OperationNode) {
            node.setRight(codeFolding((OperationNode) node.getRight()));
        }
        //if they are unary operation
        if(node.getLeft() instanceof UnaryOperationNode){
            node.setLeft(checkExpression(node.getLeft()));
        }
        if(node.getRight() instanceof UnaryOperationNode){
            node.setRight(checkExpression(node.getRight()));
        }
        //if it's a value node
        if (node.getLeft() instanceof ValueNode && node.getRight() instanceof ValueNode) {

            boolean changeBack = false;

            if(node.getLeft().getDataType() == TokenType.INTEGER && node.getRight().getDataType() == TokenType.INTEGER)
                changeBack = true;

            double leftValue = Double.parseDouble(((ValueNode) node.getLeft()).getAttribute());
            double rightValue = Double.parseDouble(((ValueNode) node.getRight()).getAttribute());

            ValueNode vn = null;
            switch( node.getOperation()){

                case PLUS:
                    vn = new ValueNode("" + (leftValue + rightValue));
                    break;

                case MINUS:
                    vn = new ValueNode("" + (leftValue - rightValue));
                    break;

                case DIVIDE:
                    vn = new ValueNode("" + (leftValue / rightValue));
                    break;

                case TIMES:
                    vn = new ValueNode("" + (leftValue * rightValue));
                    break;

                case MOD:
                    vn = new ValueNode("" + (leftValue % rightValue));
                    break;

                case DIV:
                    vn = new ValueNode("" + (int)(leftValue / rightValue));
                    break;

                case AND:
                    if((leftValue==1)&&(rightValue==1)){
                        vn = new ValueNode(""+1);
                    }
                    else
                        vn = new ValueNode("" + 0);
                    break;

                case OR:
                    if((leftValue == 1)||(rightValue==1)) {
                        vn = new ValueNode("" + 1);
                    }
                    else
                        vn = new ValueNode("" + 0);
                    break;

                case EQUALITY_OPERATOR:
                    if(leftValue == rightValue)
                        vn = new ValueNode("1");
                    else
                        vn = new ValueNode("0");
                    break;

                case NOT_EQUAL:
                    if(leftValue != rightValue)
                        vn = new ValueNode("1");
                    else
                        vn = new ValueNode("0");
                    break;

                case LESS_THAN:
                    if(leftValue < rightValue)
                        vn = new ValueNode("1");
                    else
                        vn = new ValueNode("0");
                    break;

                case LESS_THAN_EQUAL_TO:
                    if(leftValue <= rightValue)
                        vn = new ValueNode("1");
                    else
                        vn = new ValueNode("0");
                    break;

                case GREATER_THAN:
                    if(leftValue > rightValue)
                        vn = new ValueNode("1");
                    else
                        vn = new ValueNode("0");
                    break;

                case GREATER_THAN_EQUAL_TO:
                    if(leftValue >= rightValue)
                        vn = new ValueNode("1");
                    else
                        vn = new ValueNode("0");
                    break;
                default:
                    return null;
            }

            //if both expressions are integers
            if(changeBack){
                return new ValueNode("" +(int)Double.parseDouble(vn.getAttribute()));
            }

            return vn;
        } else {
            return node;
        }
    }

    /**
     * Checks to make sure the function has the proper number of arguments
     * @param fctn to check the argument number of
     * @return true if the numbers match. False if they don't
     */
    private boolean hasValidArgs(FunctionNode fctn){
        String name = fctn.getName();
        for(SubProgramNode spn: root.getFunctions().getProcs()){
            if(spn.getName().equals(name)){
                if((spn.getArguments().size())==(fctn.getExpNode().size()+1)){
                    return true;
                }
                else return false;
            }
        }
        return false;
    }
}
