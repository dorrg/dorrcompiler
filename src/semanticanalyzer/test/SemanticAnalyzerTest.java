package semanticanalyzer.test;

import org.junit.Test;
import semanticanalyzer.SemanticAnalyzer;
import syntaxtree.*;
import scanner.TokenType;

/**
 * Created by Greg on 4/5/2017.
 */
public class SemanticAnalyzerTest {
//      @Test
//    public void foldVars() throws Exception {
//        SemanticAnalyzer sa = new SemanticAnalyzer(new ProgramNode("TEST"));
//
//        //Testing standard variable
//        VariableNode var = new VariableNode("foo");
//        VariableNode result = sa.foldVars(var);
//        String expected = "foo";
//        assertEquals(result.toString(),expected);
//
//
//        System.out.println("------ArrayNode[5+8]------");
//        //Testing ArrayCall 5 + 8
//        ArrayNode array = new ArrayNode("array", TokenType.REAL);
//        //setting up the operation
//        OperationNode op = new OperationNode(TokenType.PLUS);
//        op.setLeft(new ValueNode("5"));
//        op.setRight(new ValueNode("8"));
//        //adding it to the node
//        array.setExpNode(op);
//        //saving the resulting number of lines
//        int original = array.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + array.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldVars(array);
//        int end = array.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + array.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//
//        System.out.println("\n------ArrayNode[8+8+8+8]------");
//
//        //Testing ArrayCall 5 + 8
//         array = new ArrayNode("array", TokenType.REAL);
//        //setting up the operation
//        op = new OperationNode(TokenType.PLUS);
//            //nested statement
//            OperationNode op2 = new OperationNode(TokenType.PLUS);
//            op2.setLeft(new ValueNode("8"));
//            op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//
//        //adding it to the node
//        array.setExpNode(op);
//        //saving the resulting number of lines
//        original = array.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + array.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldVars(array);
//        end = array.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + array.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//
//
//        System.out.println("\n------FunctionNode(5+8,8)------");
//        //Testing ArrayCall 5 + 8
//        FunctionNode fctn = new FunctionNode("fctn", TokenType.REAL);
//        //setting up the operation
//        op = new OperationNode(TokenType.PLUS);
//        op.setLeft(new ValueNode("5"));
//        op.setRight(new ValueNode("8"));
//        //adding it to the node
//        fctn.addExpNode(op);
//        fctn.addExpNode(new ValueNode("8"));
//        //saving the resulting number of lines
//        original = fctn.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + fctn.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldVars(fctn);
//        end = fctn.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + fctn.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//
//        System.out.println("\n------FunctionNode(5+8,8)------");
//        //Testing ArrayCall 5 + 8
//        fctn = new FunctionNode("fctn", TokenType.REAL);
//        //setting up the operation
//        op = new OperationNode(TokenType.PLUS);
//            //nested statement
//            op2 = new OperationNode(TokenType.PLUS);
//            op2.setLeft(new ValueNode("8"));
//            op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        //adding it to the node
//        fctn.addExpNode(op);
//        fctn.addExpNode(new ValueNode("8"));
//        //saving the resulting number of lines
//        original = fctn.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + fctn.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldVars(fctn);
//        end = fctn.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + fctn.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//    }
//
//    @Test
//    public void foldStatement() throws Exception {
//        SemanticAnalyzer sa = new SemanticAnalyzer(new ProgramNode("greg"));
//
//    //ASSIGNMENTNODE
//        System.out.println("\n------AssignmentStatementNode------");
//        //expression node
//        OperationNode op = new OperationNode(TokenType.PLUS);
//            //nested statement
//            OperationNode op2 = new OperationNode(TokenType.TIMES);
//            op2.setLeft(new ValueNode("8"));
//            op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        //assignment statement node
//        AssignmentStatementNode assign = new AssignmentStatementNode();
//        assign.setLvalue(new VariableNode("Test"));
//        assign.setExpression(op);
//
//        int original = assign.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + assign.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldStatement(assign);
//        int end = assign.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + assign.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//
//    //Assignment2
//        System.out.println("\n------AssignmentStatementNode------");
//        //expression node
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        //assignment statement node
//        assign = new AssignmentStatementNode();
//
//        //Testing ArrayCall 5 + 8
//        ArrayNode array = new ArrayNode("array", TokenType.REAL);
//        //setting up the operation
//        //adding it to the node
//        array.setExpNode(op);
//        assign.setLvalue(array);
//        assign.setExpression(new ValueNode("10.5"));
//
//        original = assign.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + assign.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldStatement(assign);
//        end = assign.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + assign.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//        //Assignment3
//        System.out.println("\n------AssignmentStatementNode------");
//        //expression node
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        //assignment statement node
//        assign = new AssignmentStatementNode();
//
//        //Testing ArrayCall 5 + 8
//        array = new ArrayNode("array", TokenType.REAL);
//        //setting up the operation
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//
//        //adding it to the node
//        array.setExpNode(op);
//        assign.setLvalue(array);
//        assign.setExpression(op);
//
//        original = assign.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + assign.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldStatement(assign);
//        end = assign.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + assign.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//
//
//
//    //IfStatementNode
//        System.out.println("\n------IfStatementNode------");
//        //ifstatement
//        OperationNode ifop = new OperationNode(TokenType.LESS_THAN);
//        ifop.setLeft(new VariableNode("varNode",TokenType.INTEGER));
//        ifop.setRight(new VariableNode("5"));
//
//        //Then statement
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//            op2 = new OperationNode(TokenType.PLUS);
//            op2.setLeft(new ValueNode("8"));
//            op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        AssignmentStatementNode thenState = new AssignmentStatementNode();
//        thenState.setLvalue(new VariableNode("var2",TokenType.INTEGER));
//        thenState.setExpression(op);
//
//        //else statement
//        OperationNode elseop = new OperationNode(TokenType.PLUS);
//        elseop.setLeft(new ValueNode("5"));
//        elseop.setRight(new VariableNode("5"));
//        AssignmentStatementNode elseState = new AssignmentStatementNode();
//        elseState.setLvalue(new VariableNode("array",TokenType.INTEGER));
//        elseState.setExpression(elseop);
//
//        //if statement node
//        IfStatementNode ifstate = new IfStatementNode();
//        ifstate.setTest(ifop);
//        ifstate.setThenStatement(thenState);
//        ifstate.setElseStatement(elseState);
//
//        original = ifstate.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + ifstate.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldStatement(ifstate);
//        end = ifstate.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + ifstate.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//        //procedureStatementNode
//        System.out.println("\n------ProcedureStatementNode------");
//        //statement1
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//            op2 = new OperationNode(TokenType.PLUS);
//            op2.setLeft(new ValueNode("8"));
//            op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//
//        //statement2
//        elseop = new OperationNode(TokenType.PLUS);
//        elseop.setLeft(new ValueNode("5"));
//        elseop.setRight(new VariableNode("5"));
//
//    //ProcedureStatementNode
//        ProcedureStatementNode psn = new ProcedureStatementNode();
//        psn.setVariable("var");
//        psn.addExpNode(op);
//        psn.addExpNode(elseop);
//
//        original = psn.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + psn.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldStatement(psn);
//        end = psn.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + psn.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//    //while statement node
//        System.out.println("\n------IfStatementNode------");
//        //ifstatement
//        ifop = new OperationNode(TokenType.LESS_THAN);
//        ifop.setLeft(new VariableNode("varNode",TokenType.INTEGER));
//        ifop.setRight(new ValueNode("5"));
//
//        //Do statement
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        AssignmentStatementNode doState = new AssignmentStatementNode();
//        doState.setLvalue(new VariableNode("var2",TokenType.INTEGER));
//        doState.setExpression(op);
//
//        //if statement node
//        WhileStatementNode whilestate = new WhileStatementNode();
//        whilestate.setTest(ifop);
//        whilestate.setDoStatement(doState);
//
//        original = whilestate.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + whilestate.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldStatement(whilestate);
//        end = whilestate.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + whilestate.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
////compoundstatementNode
//    //ASSIGNMENTNODE
//        System.out.println("\n------CompoundStatementNode------");
//        //expression node
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        //assignment statement node
//        assign = new AssignmentStatementNode();
//        assign.setLvalue(new VariableNode("Test"));
//        assign.setExpression(op);
//
//    //IfStatementNode
//        //ifstatement
//        ifop = new OperationNode(TokenType.LESS_THAN);
//        ifop.setLeft(new VariableNode("varNode",TokenType.INTEGER));
//        ifop.setRight(new VariableNode("5"));
//
//        //Then statement
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        thenState = new AssignmentStatementNode();
//        thenState.setLvalue(new VariableNode("var2",TokenType.INTEGER));
//        thenState.setExpression(op);
//
//        //else statement
//        elseop = new OperationNode(TokenType.PLUS);
//        elseop.setLeft(new ValueNode("5"));
//        elseop.setRight(new VariableNode("5"));
//        elseState = new AssignmentStatementNode();
//        elseState.setLvalue(new VariableNode("array",TokenType.INTEGER));
//        elseState.setExpression(elseop);
//
//        //if statement node
//        ifstate = new IfStatementNode();
//        ifstate.setTest(ifop);
//        ifstate.setThenStatement(thenState);
//        ifstate.setElseStatement(elseState);
//
//    //procedureStatementNode
//        //statement1
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//
//        //statement2
//        elseop = new OperationNode(TokenType.PLUS);
//        elseop.setLeft(new ValueNode("5"));
//        elseop.setRight(new VariableNode("5"));
//
//        //ProcedureStatementNode
//        psn = new ProcedureStatementNode();
//        psn.setVariable(new VariableNode("var"));
//        psn.addExpNode(op);
//        psn.addExpNode(elseop);
//
//    //while statement node
//        //ifstatement
//        ifop = new OperationNode(TokenType.LESS_THAN);
//        ifop.setLeft(new VariableNode("varNode",TokenType.INTEGER));
//        ifop.setRight(new VariableNode("5"));
//
//        //Do statement
//        op = new OperationNode(TokenType.PLUS);
//        //nested statement
//        op2 = new OperationNode(TokenType.PLUS);
//        op2.setLeft(new ValueNode("8"));
//        op2.setRight(new ValueNode("8"));
//        op.setLeft(op2);
//        op.setRight(op2);
//        doState = new AssignmentStatementNode();
//        doState.setLvalue(new VariableNode("var2",TokenType.INTEGER));
//        doState.setExpression(op);
//
//        //if statement node
//        whilestate = new WhileStatementNode();
//        whilestate.setTest(ifop);
//        whilestate.setDoStatement(doState);
//
//    //ACTUAL TEST
//        CompoundStatementNode csn = new CompoundStatementNode();
//        csn.addStatement(assign);
//        csn.addStatement(ifstate);
//        csn.addStatement(psn);
//        csn.addStatement(whilestate);
//
//        original = csn.indentedToString(0).split("\n").length;
//        System.out.println("Original:\n\t" + csn.indentedToString(0).replaceAll("\n","\n\t"));
//        //calling the fold
//        sa.foldStatement(csn);
//        end = csn.indentedToString(0).split("\n").length;
//        System.out.println("New:\n\t" + csn.indentedToString(0).replaceAll("\n","\n\t"));
//        System.out.println("Difference: " + (original-end));
//
//
//
//    }
//
//    @Test
//    public void checkExpression() throws Exception {
//    }


    @Test
    public void testUnaryOperation() throws Exception{

        OperationNode op2;
        OperationNode op = new OperationNode(TokenType.PLUS);
        //nested statement
            op2 = new OperationNode(TokenType.PLUS);
            op2.setLeft(new ValueNode("8"));
            op2.setRight(new ValueNode("8"));
        op.setLeft(op2);
        UnaryOperationNode uni = new UnaryOperationNode(TokenType.PLUS);
        uni.setExpression(new ValueNode("5"));
        op.setRight(uni);

        SemanticAnalyzer sa = new SemanticAnalyzer(new ProgramNode("hello"));

        System.out.println(sa.unaryFolding(uni));
        System.out.println(uni);
    }

}