package parser;

import scanner.MyScanner;
import symboltable.SymbolTable;
import syntaxtree.*;
import scanner.Token;
import scanner.TokenType;
import java.io.*;
import java.util.ArrayList;

/**
 * The parser recognizes input tokens taken from the scanner.
 * It is a recursive decent parser. It also will build a symbolTable
 * of the id's of functions, variables, arrays, programs, and procedures.
 *
 * @author Greg Dorr
 */
@SuppressWarnings("WeakerAccess")
public class Parser {
    private Token lookahead;                //The current token from the scanner
    private final MyScanner scanner;        //Scanner that scans the .pas file
    private SymbolTable symbolTable;       //SymbolTable to hold the ids
    private ProgramNode program;

    /**
     * Constructor
     * @param text either path to the file or a string of text to be passed through
     * @param isFilename trus if it's a file being passed it. otherwise false.
     */
    public Parser( String text, boolean isFilename) {
        if( isFilename) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(text);
            } catch (FileNotFoundException ex) {
                error( "No file");
            }
            assert fis != null;
            InputStreamReader isr = new InputStreamReader( fis);
            scanner = new MyScanner( isr);
        }
        else {
            scanner = new MyScanner( new StringReader( text));
        }
        try {
            lookahead = scanner.nextToken();
        } catch (IOException ex) {
            error( "Scan error");
        }
        symbolTable = new SymbolTable();
    }

    /**
     * Matches the expected token.
     * If the current token in the input stream from the scanner
     * matches the token that is expected, the current token is
     * consumed and the scanner will move on to the next token
     * in the input.
     * The null at the end of the file returned by the
     * scanner is replaced with a fake token containing no
     * type.
     *
     * @param expected The expected token type.
     */
    @SuppressWarnings("WeakerAccess")
    public void match(TokenType expected) {
        if (this.lookahead.getType() == expected) {

            //checks the token to see if it should be added to the idTable array
            //checkToken();

            try {
                this.lookahead = scanner.nextToken();
                if (this.lookahead == null) {
                    this.lookahead = new Token("End of File", TokenType.EOF);
                }
            } catch (IOException ex) {
                error("scanner exception");
            }
        } else {
            error("Match of " + expected + " found " + this.lookahead.getType()
                    + " instead at line " + lookahead.getLineNumber());
        }
    }

    /**
     * Prints a message passed into it to standard err
     *
     * @param message to be displayed
     */
    public void error(String message) {
        System.err.println("Error " + message);
    }


    /**
     * This is the compiler function of the parser.
     * It will create a syntax Tree representation of the code
     * a program consists of a name, optional variables,
     * optional functions/procedures, and a compound body.
     * It will end with a period at the end
     * @return ProgramNode for the syntax tree
     */
    public ProgramNode program() {
        match(TokenType.PROGRAM);
        //pushing a new scope on the stack
        symbolTable.addNewScope();
        //name of the program
        String name = lookahead.getLexeme();
        //creating a new ProgramNode and naming it with the program's name
        program = new ProgramNode(name);

        match(TokenType.ID);
        match(TokenType.SEMI_COLON);

        //adding the program to the symbol table
        symbolTable.addID(name,null,TokenType.PROGRAM,0,0);

        //setting the variables
        program.setVariables(declarations());

        //setting the functions (if any)
        program.setFunctions(subprogram_declarations());

        //setting the main body of the program
        program.setMain(compound_statement());

        match(TokenType.PERIOD);

        return program;
    }

    /**
     * Matches ID.
     * checks to see if there is a comma.
     * if there is it will match it, and call identifier_list.
     * else it will do nothing.
     *
     * @return an ArrayList of Strings containing the variable names
     */
    public ArrayList<String> identifier_list() {

        //adds all the names to a list
        ArrayList<String> names = new ArrayList<>();

        //adding names to a list
        names.add(lookahead.getLexeme());

        match(TokenType.ID);
        if (lookahead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            //when it recursively calls its self, it will return a declartions node
            //that node will add it's contents to the current levels declaration node
            names.addAll(identifier_list());
        }
        return names;
    }
    /**
     * Checks to see if the token is a VAR.
     * if it is, it will match the var, call
     * identifier_list, match the colon token,
     * call type, match the semi_colon, and finally
     * call declarations.
     * else it will do nothing
     * @return DeclarationNode
     */
    public DeclarationsNode declarations() {
        //creating a new declaration node to return to the program node
        DeclarationsNode dnode = new DeclarationsNode();
        if (lookahead.getType() == TokenType.VAR) {
            match(TokenType.VAR);

            //adds all the variables name to the list
            ArrayList<String>names=(identifier_list());

            match(TokenType.COLON);

            //passing the names into the types
            TokenType type = type(names);

            //for each loop to add all the data types to the names
            //adding it to the declarations node
            for(String name: names){
                dnode.addVariable(new VariableNode(name,type));
            }
            match(TokenType.SEMI_COLON);

            dnode.addDeclarations(declarations());
        }
        return dnode;
    }

    /**
     * Checks to see if the type is an array.
     * if it is an array, it will match the tokens: Array,
     * Left_square_bracket, a number, colon, another number,
     * the right_square_bracket, and finally of. it will then
     * call standard_type().
     * else it's not an array, it will just call standard_type
     * @param id ArrayList of Ids to set the symbolTable
     * @return Return type of function or Variable type
     */
    public TokenType type(ArrayList<String> id) {
        //the token type of the expression

        //checking if it's an array
        if (lookahead.getType() == TokenType.ARRAY) {
            match(TokenType.ARRAY);
            match(TokenType.LEFT_SQUARE_BRACKET);
            //start index of the array
            int startIndex = Integer.parseInt(lookahead.getLexeme());
            match(TokenType.NUMBER);
            match(TokenType.COLON);
            //end index of the array
            int endIndex = Integer.parseInt(lookahead.getLexeme());
            match(TokenType.NUMBER);
            match(TokenType.RIGHT_SQUARE_BRACKET);
            match(TokenType.OF);
            //adding the array to the symbol table
            TokenType returnType = standard_type();
            for(String var:id){
                symbolTable.addID(var,returnType,TokenType.ARRAY,startIndex,endIndex);
            }
            return returnType;
            //they are just simple variables
        } else {
            TokenType returnType = standard_type();
            //else add the variables to the symbol table
            for(String var:id){
                symbolTable.addID(var,returnType,TokenType.VAR,0,0);
            }
            return returnType;
        }
    }

    /**
     * Checks to see if the token is an integer or real
     * it will match integer or real depending on which
     * the token is.
     * else it will throw an error saying "not a standard_type"
     * @return TokenType
     */
    public TokenType standard_type() {
        if (lookahead.getType() == TokenType.INTEGER) {
            match(TokenType.INTEGER);
            return TokenType.INTEGER;
        } else if (lookahead.getType() == TokenType.REAL) {
            match(TokenType.REAL);
            return TokenType.REAL;
        } else {
            error("not a standard_type");
        }
        return null;
    }

    /**
     * lookahead looks to see if the next token is a function of a procedure
     * if it is one of the two, it'll enter the if statement and continually
     * recursively call it's self until there's no more subprogram declarations
     * @return SubProgramDeclarationsNode - containing functions or procedures
     */
    public SubProgramDeclarationsNode subprogram_declarations() {

        SubProgramDeclarationsNode spdnode = new SubProgramDeclarationsNode();

        if (lookahead.getType() == TokenType.FUNCTION || lookahead.getType() == TokenType.PROCEDURE) {

            spdnode.addSubProgramDeclaration(subprogram_declaration());

            match(TokenType.SEMI_COLON);

            //creating the subprogram_declaration
            spdnode.addall(subprogram_declarations().getProcs());

        }
        return spdnode;
    }

    /**
     * a subprogram_declaration calls subprogram_head,
     * declarations, subprogram_declarations and compound_statement
     * @return SubProgramNode - containing an individual function or procedure
     */
    public SubProgramNode subprogram_declaration() {
        //subprogram_head
        SubProgramNode spNode = subprogram_head();
        //subprogram variables
        spNode.setVariables(declarations());
        //adding the arguments to the symbolTable
        for(VariableNode var: spNode.getArguments()){
            if(var instanceof VariableNode){
                symbolTable.addID(var.getName(),var.getDataType(),TokenType.VAR,0,0);
            }
        }
        //adds the name of the function to the symbol table for the return
        if(spNode.getIsFunction()){
            //symbolTable.addID(spNode.getName().toUpperCase(),spNode.getReturnType(),TokenType.VAR,0,0);
        }

        //subprogram functions
        spNode.setFunctions(subprogram_declarations());

        //subprogram compound statement
        spNode.setMain(compound_statement());

        symbolTable.setLocalTable(spNode.getName(),symbolTable.removeScope());

        return spNode;
    }

    /**
     * subprogram_head checks for the token function or a procedure
     * if it's a function, it will match function and id. it will then
     * call arguments to check if there are any arguments. a Colon is then matched
     * followed by a call to type and a match of the semi colon.
     *
     * if its a procedure, it will match the token procedure and the id.
     * it will then call arguments to check if there are any. followed by
     * the match of semi_colon
     *
     * else it wont do anything.
     * @return SubProgramNode - containing an individual function or procedure
     */
    public SubProgramNode subprogram_head() {
        //creating a new subprogramNode
        SubProgramNode spNode = null;

        if (lookahead.getType() == TokenType.FUNCTION) {
            match(TokenType.FUNCTION);
            //saving the name of the function
            String name = lookahead.getLexeme();
            //creating the subprogram node
            spNode = new SubProgramNode(name,true);
            match(TokenType.ID);
            //adding the function to the current symbolTable
            symbolTable.simpleAdd(name,TokenType.FUNCTION);

            //creating a new scope
            symbolTable.addNewScope();

            //adding the arguments to the new symbol table
            spNode.addAllArguments(arguments());
            spNode.addArgument(new VariableNode(spNode.getName(),spNode.getReturnType()));

            match(TokenType.COLON);
            TokenType returnType = standard_type();
            spNode.setReturnType(returnType);
            match(TokenType.SEMI_COLON);

            //adding the type to the table
            symbolTable.addType(name,returnType);

            //add a special Symbol to the table that holds the functions name
            symbolTable.addID(name, returnType, TokenType.VAR, 0,0);
        }
        else if(lookahead.getType() == TokenType.PROCEDURE){
            match(TokenType.PROCEDURE);
            //saving the name of the procedure
            String name = lookahead.getLexeme();
            //adding the procedure to the symbol table
            symbolTable.addID(name,null,TokenType.PROCEDURE,0,0);
            symbolTable.addNewScope();
            //creating the subprogram node
            spNode = new SubProgramNode(name,false);
            match(TokenType.ID);
            spNode.addAllArguments(arguments());
            match(TokenType.SEMI_COLON);

        }
        else{
            error("not a subprogram_head");
        }
        return spNode;
    }

    /**
     * if the token while this class is called a left_parentheses,
     * it will match it, call parameter_list and match the right_parentheses
     * else it wont do anything if there aren't any arguments
     * @return an ArrayList of Variable Nodes that are represent the arguments of a function or procedure
     */
    public ArrayList<VariableNode> arguments() {
        if (lookahead.getType() == TokenType.LEFT_PARENTHESES) {
            match(TokenType.LEFT_PARENTHESES);
            ArrayList<VariableNode> args = parameter_list();
            match(TokenType.RIGHT_PARENTHESES);
            return args;
        }
        return new ArrayList<>();
    }

    /**
     * Parameter_list contains an identifier_list
     * it then matches the token Colon, calls Type,
     * checks if the next token is a semi colon, if it is
     * it matches the semi_colon, then calls it's self
     * recursively.
     * @return an ArrayList of Variable Nodes
     */
    public ArrayList<VariableNode> parameter_list() {
        //variable nodes to add to the arguments
        ArrayList<VariableNode> var = new ArrayList();

        // call to identifier list
        ArrayList<String> ids = identifier_list();
        match(TokenType.COLON);

        //call to type
        TokenType tokenType = type(ids);
        for(String str: ids){
            var.add(new VariableNode(str,tokenType));
        }

        if (lookahead.getType() == TokenType.SEMI_COLON) {
            match(TokenType.SEMI_COLON);
            var.addAll(parameter_list());
        }
        return var;
    }

    /**
     * a compound_statement contains the token
     * begin an optional_statement and the end token
     * @return CompoundStatementNode - containing Optional Statements
     */
    public CompoundStatementNode compound_statement() {
        match(TokenType.BEGIN);
        CompoundStatementNode csNode = optional_statements();
        match(TokenType.END);
        return csNode;
    }

    /**
     * Optional_statement checks if the next token is a statement.
     * If it is, it will add all statements returned from statement_list()
     * into a new CompoundStatementNode
     * @return CompoundStatementNode - containing all statements
     */
    public CompoundStatementNode optional_statements() {
        CompoundStatementNode csNode = new CompoundStatementNode();
        if(isStatement()){
            csNode.addAllStatements(statement_list());
        }
        return csNode;
    }

    /**
     *  statement_list calls statement
     *  If there is a semi colon after a statement,
     *  it will recursively call it's self until there
     *  isn't any more.
     *  @return ArrayList of StatementNodes
     */
    public ArrayList<StatementNode> statement_list() {
        ArrayList<StatementNode> statementList = new ArrayList<>();
        statementList.add(statement());
        if (lookahead.getType() == TokenType.SEMI_COLON) {
            match(TokenType.SEMI_COLON);
            statementList.addAll(statement_list());
        }
        return statementList;
    }

    /**
     * Statement checks for validity in the statement. a valid statement could be
     * a variable assignment, procedure_statement, compound_statement, if statement,
     * while statement, read or write.
     * @return StatementNode - could be: AssignmentNode, ProcedureStatementNode, CompoundStatementNode,
     *
     */
    public StatementNode statement() {
        //check if it's a variable or procedure
        if (lookahead.getType() == TokenType.ID) {
            //if its a variable, it will call variable which will return a varNode
            // assignop, and expression-which returns and expression node.
            // it'll then add the value node and expression node to a new assignment node and return that
            if(symbolTable.isVariableName(lookahead.getLexeme()) || symbolTable.isArrayName(lookahead.getLexeme())){
                AssignmentStatementNode assignNode = new AssignmentStatementNode();
                assignNode.setLvalue(variable());
                assignop();
                assignNode.setExpression(expression());

                //checking if it's NOT a valid assignment
                if(!isValidAssignment(assignNode))
                    error("invalid assignment: " + assignNode.getLvalue().toString().toUpperCase()+" is a "+assignNode.getLvalue().getDataType() + " not a " + assignNode.getExpression().getDataType());
                else if(isDeclared(assignNode.getLvalue()))
                    error("Variable: " + assignNode.getLvalue().toString().toUpperCase()+ " is not declared");

                return assignNode;
            }
            //else it's a procedure statement
            else if(symbolTable.isProcedureName(lookahead.getLexeme())){
                return procedure_statement();
            }
            else{
                error("***"+lookahead.getLexeme()+"*** Not a Variable or Procedure Name");
            }
        }

        //compound_statement
        else if(lookahead.getType()==TokenType.BEGIN){
            return compound_statement();
        }
        // theres an if statement
        else if (lookahead.getType() == TokenType.IF) {
            //ifstatement node
            IfStatementNode ifNode = new IfStatementNode();
            match(TokenType.IF);
            //setting the test
            ifNode.setTest(expression());
            match(TokenType.THEN);
            //setting the then
            ifNode.setThenStatement(statement());
            match(TokenType.ELSE);
            //setting the else
            ifNode.setElseStatement(statement());
            return ifNode;
        }
        //setting a while statement
        else if (lookahead.getType() == TokenType.WHILE) {
            WhileStatementNode wsNode = new WhileStatementNode();
            match(TokenType.WHILE);
            //setting the condition
            wsNode.setTest(expression());
            match(TokenType.DO);
            //setting the body
            wsNode.setDoStatement(statement());
             return wsNode;
        }

        else if(lookahead.getType() == TokenType.READ){
            match(TokenType.READ);
            match(TokenType.LEFT_PARENTHESES);
            String varName = lookahead.getLexeme();
            if(!symbolTable.isVariableName(varName)){
                error("Variable not in SymbolTable on line: "+lookahead.getLineNumber());
            }
            match(TokenType.ID);
            match(TokenType.RIGHT_PARENTHESES);
            return new ReadNode(new VariableNode(varName));
        }

        else if(lookahead.getType() == TokenType.WRITE){
            match(TokenType.WRITE);
            match(TokenType.LEFT_PARENTHESES);
            WriteNode wn = new WriteNode();
            wn.setExpNode(expression());
            match(TokenType.RIGHT_PARENTHESES);
            return wn;
        }

        //will never get to this point
        return null;
    }

    /**
     * Variable checks the current token and matches the id.
     * it then checks if there is a left square bracket <em>[</em>.
     * If there is one, it will create a new arrayNode and return that at
     * the position expressed in the expression
     *  @return VariableNode or ArrayNode depending on the token
     */
    public VariableNode variable() {
        String varName = lookahead.getLexeme();

        VariableNode vn = new VariableNode(varName,symbolTable.getReturnType(varName));
        match(TokenType.ID);
        //create a return a new statement node
        if (lookahead.getType() == TokenType.LEFT_SQUARE_BRACKET) {
            //creating a new arraynode
            //setting the name to be the variable name above
            ArrayNode arraynode = new ArrayNode(vn.getName(),symbolTable.getReturnType(vn.getName()));
            match(TokenType.LEFT_SQUARE_BRACKET);
            //setting the expression to be the result of expression()
            arraynode.setExpNode(expression());
            match(TokenType.RIGHT_SQUARE_BRACKET);
            return arraynode;
        }
        return vn;
    }

    /**
     *  procedure_statement will create anew procedure statement in
     *  order to call or use a procedure statement
     *  @return ProcedureStatementNode - containing the procedure name and all expressions
     */
    public ProcedureStatementNode procedure_statement() {
        //creating a new procedure statement node
        ProcedureStatementNode psnode = new ProcedureStatementNode();
        //setting the variable
        psnode.setVariable((lookahead.getLexeme()));
        match(TokenType.ID);
        if (lookahead.getType() == TokenType.LEFT_PARENTHESES) {
            match(TokenType.LEFT_PARENTHESES);
            psnode.addAllExpNode(expression_list());
            match(TokenType.RIGHT_PARENTHESES);
            if(!hasValidArgs(psnode)){
                error("Invalid arguments");
            }
        }
        return psnode;
    }

    /**
     *an expression_list is an expression.
     * if the next token is a comma, it can also be
     * an expression, comma, and an expression_list
     * @return ArrayList of ExpressionNodes
     */
    public ArrayList<ExpressionNode> expression_list() {
        //arraylist to hold the expression nodes
        ArrayList<ExpressionNode> exp = new ArrayList<>();
        exp.add(expression());
        if (lookahead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            exp.addAll(expression_list());
        }
        return exp;
    }

    /**
     * checks to see if theres a relop. calls simple_expression no matter what
     * an expression can be a simple_expression
     * It can also be a simple_expression, relop and a simple_expression
     *  @return ExpressionNode - either a new relop OperationNode with the simple_expression set to the left, or
     * it will just return the simple_expression.
     */
    public ExpressionNode expression() {
        //expression node
        ExpressionNode left = simple_expression();
        if (isRelop()) {
            OperationNode opNode = relop();
            //setting the left
            opNode.setLeft(left);
            //setting the right
            opNode.setRight(simple_expression());
            return opNode;
        }
        //return the left if there is not a relop
        return left;
    }

    /**
     * checks to see if the current token is a plus or a minus.
     * if it is a sign, a simple expression is a sign, term and a simple_part.
     * if it's not a sign, it's just a term and a simple_part
     * @return ExpressionNode - containing either a UnaryOperationNode an expressionNode
     */
    public ExpressionNode simple_expression() {
        //if theres a sign
        if (lookahead.getType() == TokenType.PLUS || lookahead.getType() == TokenType.MINUS) {
            //positive or negative
            UnaryOperationNode opNode = sign();
            ExpressionNode expNode = term();
            opNode.setExpression(simple_part(expNode));
            return opNode;
            //else there isn't a sign
        } else {
            ExpressionNode expNode = term();
            return simple_part(expNode);
        }
    }

    /**
     * if its a addop, it'll call addop,
     * term, and simple part.
     * else lambada
     * @param possibleLeft The potential left of an expression
     * @return ExpressionNode - either a new addop OperationNode with the possibleLeft set to the left, or
     * it will just return the possible left.
     */
    public ExpressionNode simple_part(ExpressionNode possibleLeft) {

        if (isAddop()) {
            //opNode returned from addop
            OperationNode opNode = addop();
            //left is whats passed in
            opNode.setLeft(possibleLeft);
            //possible right is whats returned form term
            ExpressionNode possibleRight = term();
            opNode.setRight(simple_part(possibleRight));
            return opNode;
        }
        //else just return the possible left
        return possibleLeft;
    }

    /**
     * a term is a factor and a term_part
     * @return ExpressionNode
     */
    public ExpressionNode term() {
        ExpressionNode expNode = factor();
        return term_part(expNode);
    }

    /**
     * @param possibleLeft The possible left of a operationNode
     * If the lookahead is a mulop, it will call mulop,
     * factor, and recursively call it's self.
     * If the lookahead is not a mulop, it'll end the function
     * @return ExpressionNode - either a new mulop OperationNode with the possibleLeft set to the left, or
     * it will just return the possible left.
     */
    public ExpressionNode term_part(ExpressionNode possibleLeft) {

       //if theres a mulop it will set the possible left to the left of a
        // newly created opNode. it will then recursivly call it's self to set
        // the left/ right...
        if (isMulop()) {
            //creating a new op node if theres a mulop
            OperationNode opNode = mulop();
            //setting the left to what was passed in.
            opNode.setLeft(possibleLeft);
            //saving the response from factor to be the possible right
            ExpressionNode possibleRight = factor();
            //passing the possible right into term_Part to create the tree
            opNode.setRight(term_part(possibleRight));
            //returning the operation node
            return opNode;
        }
        //just return the left
        return possibleLeft;
    }

    /**
     * a factor can be an id, id[expression], id(expression_list),
     * number, (expression), or NOT factor.
     * @return ExpressionNode - containing the proper factor
     */
    public ExpressionNode factor() {

        ExpressionNode answer = null;

        //if it's an id
        if (lookahead.getType() == TokenType.ID) {
            //saving the id to a variable node
            String varName = lookahead.getLexeme();
            answer = new VariableNode(varName,symbolTable.getReturnType(varName));

            //matching the id
            match(TokenType.ID);

            //if its a left square bracket
            if (lookahead.getType() == TokenType.LEFT_SQUARE_BRACKET) {
                ArrayNode arraynode = new ArrayNode(varName,symbolTable.getReturnType(varName));
                match(TokenType.LEFT_SQUARE_BRACKET);
                arraynode.setExpNode(expression());
                match(TokenType.RIGHT_SQUARE_BRACKET);
                return arraynode;
            }
            //if it's a left parentheses
            else if (lookahead.getType() == TokenType.LEFT_PARENTHESES) {
                FunctionNode fNode= new FunctionNode(varName,symbolTable.getReturnType(varName));
                match(TokenType.LEFT_PARENTHESES);
                fNode.setExpNode(expression_list());
                match(TokenType.RIGHT_PARENTHESES);
                return fNode;
            }

            //making sure the id is declared
            if(isDeclared((VariableNode)answer)){
                error("Variable: "+varName.toUpperCase() +" is not declared on line: "+lookahead.getLineNumber());
            }
            return answer;
        }

        //if its a number
        else if (lookahead.getType() == TokenType.NUMBER) {
            answer = new ValueNode(lookahead.getLexeme());
            match(TokenType.NUMBER);
        }

        //its a parentheses
        else if (lookahead.getType() == TokenType.LEFT_PARENTHESES) {
            match(TokenType.LEFT_PARENTHESES);
            ExpressionNode expNode = expression();
            match(TokenType.RIGHT_PARENTHESES);
            return expNode;
        }

        //if its a not
        else if (lookahead.getType() == TokenType.NOT) {
            match(TokenType.NOT);
            UnaryOperationNode uOpNode = new UnaryOperationNode(TokenType.NOT);
            uOpNode.setExpression(factor());
            return uOpNode;
        }
        return answer;
    }

    /**
     * matches the sign. the sign can be only positive or negative
     * @return UrnaryOperationNode - with the operation set either to positive or negative.
     */
    public UnaryOperationNode sign() {

        //creating a new OpNode
        UnaryOperationNode opNode = null;

        if (lookahead.getType() == TokenType.PLUS) {
            match(TokenType.PLUS);
            opNode = new UnaryOperationNode(TokenType.PLUS);
        } else if (lookahead.getType() == TokenType.MINUS) {
            match(TokenType.MINUS);
            opNode = new UnaryOperationNode(TokenType.MINUS);
        } else {
            error("Sign error");
        }

        return opNode;
    }

    /**
     * switches through the lookahead tokenType
     * and matches on the appropriate type.
     * @return OperationNode - with the operation set to a relop.
     */
    public OperationNode relop() {
        OperationNode opNode = null;
        switch (lookahead.getType()) {
            case EQUALITY_OPERATOR:
                match(TokenType.EQUALITY_OPERATOR);
                opNode = new OperationNode(TokenType.EQUALITY_OPERATOR);
                break;
            case NOT_EQUAL:
                match(TokenType.NOT_EQUAL);
                opNode = new OperationNode(TokenType.NOT_EQUAL);
                break;
            case LESS_THAN:
                match(TokenType.LESS_THAN);
                opNode = new OperationNode(TokenType.LESS_THAN);
                break;
            case LESS_THAN_EQUAL_TO:
                match(TokenType.LESS_THAN_EQUAL_TO);
                opNode = new OperationNode(TokenType.LESS_THAN_EQUAL_TO);
                break;
            case GREATER_THAN_EQUAL_TO:
                match(TokenType.GREATER_THAN_EQUAL_TO);
                opNode = new OperationNode(TokenType.GREATER_THAN_EQUAL_TO);
                break;
            case GREATER_THAN:
                match(TokenType.GREATER_THAN);
                opNode = new OperationNode(TokenType.GREATER_THAN);
                break;
            default:
                error("Not a relop");
                break;
        }
        return opNode;
    }

    /**
     * switches through the lookahead tokenType
     * and matches on the appropriate type.
     * @return OperationNode - with the operation set to an addop.
     */
    public OperationNode addop() {

        OperationNode answer = null;

        switch (lookahead.getType()) {
            case PLUS:
                match(TokenType.PLUS);
                answer = new OperationNode(TokenType.PLUS);
                break;
            case MINUS:
                match(TokenType.MINUS);
                answer = new OperationNode(TokenType.MINUS);
                break;
            default:
                error("Not an addop");
                break;
        }

        return answer;
    }

    /**
     * switches through the lookahead tokenType
     * and matches on the appropriate type.
     * @return OperationNode - with the operation set to a mulop.
     */
    public OperationNode mulop() {

        OperationNode answer = null;

        switch (lookahead.getType()) {
            case TIMES:
                match(TokenType.TIMES);
                answer = new OperationNode(TokenType.TIMES);
                break;
            case DIVIDE:
                match(TokenType.DIVIDE);
                answer = new OperationNode(TokenType.DIVIDE);
                break;
            case DIV:
                match(TokenType.DIV);
                answer = new OperationNode(TokenType.DIV);
                break;
            case MOD:
                match(TokenType.MOD);
                answer = new OperationNode(TokenType.MOD);
                break;
            case AND:
                match(TokenType.AND);
                answer = new OperationNode(TokenType.AND);
                break;
            default:
                error("Not a Mulop");
                break;
        }

        return answer;
    }

    /**
     * checks to see if the current tokentype is the assignment operator
     * if it's not it'll throw an error saying "Not the Assignop"
     */
    public void assignop() {
        if (lookahead.getType() == TokenType.ASSIGNMENT_OPERATOR) {
            match(TokenType.ASSIGNMENT_OPERATOR);
        }
        else
            error("Not the Assignop");
    }

    /**
     * Checks to see if it's a relop.
     * relop symbols are: =, &#60;	&#62;, &#60;, &#60;=, 	&#62;= and 	&#62;
     *
     * @return True if it's a relop or it'll return false.
     */
    public boolean isRelop() {
        switch (lookahead.getType()) {
            case EQUALITY_OPERATOR:
            case NOT_EQUAL:
            case LESS_THAN:
            case LESS_THAN_EQUAL_TO:
            case GREATER_THAN_EQUAL_TO:
            case GREATER_THAN:
                return true;
            default:
                return false;
        }
    }

    /**
     * Checks to see if it's a addop.
     * Addop symbols are: + and -
     *
     * @return True if it's a addop or it'll return false.
     */
    public boolean isAddop() {
        switch (lookahead.getType()) {
            case PLUS:
            case MINUS:
                return true;
            default:
                return false;
        }
    }

    /**
     * Checks to see if it's a mulop.
     * relop symbols are: *, /, DIV, MOD, and AND
     *
     * @return True if it's a mulop or it'll return false.
     */
    public boolean isMulop() {
        switch (lookahead.getType()) {
            case TIMES:
            case DIVIDE:
            case DIV:
            case MOD:
            case AND:
                return true;
            default:
                return false;
        }
    }

    /**
     * checks to see if the next token is a statement or not
     * @return true if next token is a statement or false if it isn't.
     */
    public boolean isStatement(){
        switch(lookahead.getType()){
            case ID:
            case BEGIN:
            case IF:
            case WHILE:
            case READ:
            case WRITE:
                return true;
            default:
                return false;
        }
    }


    /**
     * returns the symbol table initialized in the parser
     * @return The symbol table
     */
    public SymbolTable getSymbolTable(){
        return symbolTable;
    }

    /**
     * Checks to make sure the assignment is valid
     * @param asn To check for validity
     * @return true if it's invalid, and false if it's valid
     */
    private boolean isValidAssignment(AssignmentStatementNode asn){
        if(asn.getLvalue().getDataType() == asn.getExpression().getDataType())
            return true;

        /* if a variable is a real type and the expression is real.
         * it will auto convert the value to be a real type
         */
        else if(asn.getLvalue().getDataType()==TokenType.REAL && asn.getExpression().getDataType()==TokenType.INTEGER) {
            return true;
        }
        else
            return false;
    }

    /**
     * Makes sure that all variables have been declare
     * before they are used
     * @param var to check if it's been declared or not
     * @return false if the symbole is in the table. and true if it is not
     */
    private boolean isDeclared(VariableNode var){
        String name = var.getName();
        if(var instanceof FunctionNode)
            return (!symbolTable.isFunctionName(name));
        else if(var instanceof ArrayNode)
            return (!symbolTable.isArrayName(name));
        else
            return !symbolTable.isVariableName(name);
    }

    /**
     * Checks to make sure the procedureStatementNode has the proper number of
     * arguments being passed in
     * @param proc the node to check it's arguments
     * @return true if they match. false if they don't
     */
    private boolean hasValidArgs(ProcedureStatementNode proc) {
        String name = proc.getVariable();
        for (SubProgramNode spn : program.getFunctions().getProcs()) {
            if (spn.getName().equals(name)) {
                if (proc.getExpNode().size() == spn.getArguments().size()) {
                    return true;
                } else
                    return false;
            }
        }

        return false;
    }
    /**
     * string version of the object
     * @return a string representation of the object
     */
    @Override
    public String toString() {
        return "Parser{" + symbolTable +'}';
    }
}
