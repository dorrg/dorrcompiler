package parser.test;

/**
 * Created by Greg on 2/4/2017.
 */
public class ParserTest {
/*
    //@Test
    public void program() throws Exception {
        System.out.println("----------ProgramTest----------");
        Parser parse = new Parser("C:\\Users\\Greg\\IdeaProjects\\dorrcompiler\\Documentation\\sample.pas",true);
        System.out.println(parse.program());
        System.out.println("SUCCESS!\n");
    }

    //@Test
    public void declarations() throws Exception {
        System.out.println("----------DeclarationsTest----------");
        //testing a simple declaration
        String sample = "var foo: integer;";
        String expResults="Name: foo (INTEGER)\n";
        Parser parse = new Parser( sample,false);
        String results=parse.declarations().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //more complicated one
        parse = new Parser("var foo,fee,fii: array[3:3] of real;" ,false);
        results=parse.declarations().indentedToString(0);

        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);
    }

    //@Test
    public void subprogram_declaration() throws Exception {
        System.out.println("----------Subprogram_declarationsTest----------");

        //function
        System.out.println("\n~~~~Function~~~~");
        String sample = "function test( foo:real ) :real;"
                        + "\n"
                        + "var foo,fee,fii: array[3:3] of real;"
                        + "begin end .";

        String expResults = "SubProgramName: test (REAL)\n" +
                "|-- Declarations\n" +
                "|-- --- Name: foo (REAL)\n" +
                "|-- --- Name: fee (REAL)\n" +
                "|-- --- Name: fii (REAL)\n" +
                "|-- SubProgramDeclarations\n" +
                "|-- Compound Statement\n";
        Parser parse = new Parser(sample,false);
        String results=parse.subprogram_declaration().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //procedure
        System.out.println("\n~~~~Procedure~~~~");
        sample = "procedure test( foo:real );"
                +"var foo,fee,fii: array[3:3] of real;"
                + "begin end .";
        expResults = "SubProgramName: test\n" +
                "|-- Declarations\n" +
                "|-- --- Name: foo (REAL)\n" +
                "|-- --- Name: fee (REAL)\n" +
                "|-- --- Name: fii (REAL)\n" +
                "|-- SubProgramDeclarations\n" +
                "|-- Compound Statement\n";
        parse = new Parser(sample,false);
        results=parse.subprogram_declaration().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);
    }

    //@Test
    public void statement() throws Exception {
        System.out.println("----------StatementTest----------");
        //creating a symboltable for Testing
        SymbolTable table = new SymbolTable();
        table.addID("foo", TokenType.INTEGER,TokenType.VAR,null,0,0);
        table.addID("arr", TokenType.INTEGER,TokenType.ARRAY,null,0,2);
        table.addID("fo", TokenType.INTEGER,TokenType.VAR,null,0,0);
        table.addID("sum", TokenType.INTEGER,TokenType.VAR,null,0,0);

        //variable option
        System.out.println("\n~~~~Variable~~~~");
        String sample = "foo := -5 ";
        String expResults = "Assignment\n"+
                            "|-- Name: foo\n"+
                            "|-- Operation: MINUS\n"+
                            "|-- --- Value: 5\n";
        Parser parse = new Parser(sample,false);
        parse.setSymbolTable(table);
        String results=parse.statement().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);


        //Array
        System.out.println("\n~~~~array~~~~");
        sample = "arr[0] := -5 ";
        expResults = "Assignment\n"+
                "|-- Name: arr\n"+
                "|-- Position: \n"+
                "|-- --- Value: 0\n"+
                "|-- Operation: MINUS\n"+
                "|-- --- Value: 5\n";
        parse = new Parser(sample,false);
        parse.setSymbolTable(table);
        results=parse.statement().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //compound_statement option
        System.out.println("\n~~~~Compound Statement~~~~");
        sample = "begin\n"+
                "foo := -5\n" +
                "end";
        expResults = "Compound Statement\n"+
                "|-- Assignment\n"+
                "|-- --- Name: foo\n"+
                "|-- --- Operation: MINUS\n"+
                "|-- --- --- Value: 5\n";
        parse = new Parser(sample,false);
        parse.setSymbolTable(table);
        results=parse.statement().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //if statement option
        System.out.println("\n~~~~If Statement~~~~");
        sample = ("if fo < 13 "
                + "then "
                + 	"fo := 13 "
                + "else "
                + 	"fo := 26");
        expResults = "If\n" +
                "|-- Operation: LESS_THAN\n" +
                "|-- --- Name: fo\n" +
                "|-- --- Value: 13\n" +
                "|-- Assignment\n" +
                "|-- --- Name: fo\n" +
                "|-- --- Value: 13\n" +
                "Then\n" +
                "|-- Assignment\n" +
                "|-- --- Name: fo\n" +
                "|-- --- Value: 26\n" +
                "Else\n";
        parse = new Parser(sample,false);
        parse.setSymbolTable(table);
        results=parse.statement().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //while option
        System.out.println("\n~~~~While Statement~~~~");
        sample = "while number>0\n"
                + "do\n"
                + "begin\n"
                + "sum := sum + number\n"
                + "end";

        expResults = "While\n" +
                "|-- Operation: GREATER_THAN\n" +
                "|-- --- Name: number\n" +
                "|-- --- Value: 0\n" +
                "Do\n"+
                "|-- Compound Statement\n" +
                "|-- --- Assignment\n"+
                "|-- --- --- Name: sum\n" +
                "|-- --- --- Operation: PLUS\n" +
                "|-- --- --- --- Name: sum\n"+
                "|-- --- --- --- Name: number\n";
        parse = new Parser(sample,false);
        parse.setSymbolTable(table);
        results=parse.statement().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);
    }

    //@Test
    public void simple_expression() throws Exception {
        System.out.println("----------Simple_ExpressionTest----------");
        //sign-less option
        System.out.println("\n~~~~signed-less~~~~");
        String sample = "5+10*30/2";
        String expResults = "Operation: PLUS\n" +
                            "|-- Value: 5\n" +
                            "|-- Operation: TIMES\n" +
                            "|-- --- Value: 10\n" +
                            "|-- --- Operation: DIVIDE\n" +
                            "|-- --- --- Value: 30\n" +
                            "|-- --- --- Value: 2\n";
        Parser parse = new Parser(sample,false);
        String results = parse.simple_expression().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //signed option
        System.out.println("\n~~~~signed~~~~");
        sample = "-foo+10*30/2";
        expResults="Operation: MINUS\n" +
                "|-- Operation: PLUS\n" +
                "|-- --- Name: foo\n" +
                "|-- --- Operation: TIMES\n" +
                "|-- --- --- Value: 10\n" +
                "|-- --- --- Operation: DIVIDE\n" +
                "|-- --- --- --- Value: 30\n" +
                "|-- --- --- --- Value: 2\n";
        parse = new Parser(sample,false);
        results=parse.simple_expression().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);
    }

    //@Test
    public void factor() throws Exception {
        System.out.println("----------FactorTest----------");
        //id option
        System.out.println("\n~~~~id~~~~");
        String sample = "sampleId";
        String expResults="Name: sampleId\n";
        Parser parse = new Parser(sample,false);
        String results=parse.factor().indentedToString(0);
        assertEquals(results,expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //id[expression] option
        System.out.println("\n~~~~id[expression]~~~~");
        sample = "sample[this+that-5]";
        expResults = "Name: sample\n"
                    +"Position: \n"
                    +"|-- Operation: PLUS\n"
                    +"|-- --- Name: this\n"
                    +"|-- --- Operation: MINUS\n"
                    +"|-- --- --- Name: that\n"
                    +"|-- --- --- Value: 5\n";
        parse = new Parser(sample,false);
        results=parse.factor().indentedToString(0);
        assertEquals(results,expResults);
        System.out.println("\n***RESULTS***\n" + results);


        //id(expression_list) option
        System.out.println("\n~~~~id(expression_list)~~~~");
        sample = "sample(this+that-5)";
        expResults = "Name: sample\n"
                     +"Arguments: \n"
                     +"|-- Operation: PLUS\n"
                     +"|-- --- Name: this\n"
                     +"|-- --- Operation: MINUS\n"
                     +"|-- --- --- Name: that\n"
                     +"|-- --- --- Value: 5\n";
        parse = new Parser(sample,false);
        results=(parse.factor().indentedToString(0));
        assertEquals(results,expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //num option
        System.out.println("\n~~~~Number~~~~");
        sample = "5";
        expResults = "Value: 5\n";
        parse = new Parser(sample,false);
        results=parse.factor().indentedToString(0);
        assertEquals(results,expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //(expression) option
        System.out.println("\n~~~~(expression)~~~~");
        sample = "(this+that-5)";
        expResults = "Operation: PLUS\n"
                    +"|-- Name: this\n"
                    +"|-- Operation: MINUS\n"
                    +"|-- --- Name: that\n"
                    +"|-- --- Value: 5\n";
        parse = new Parser(sample,false);
        results=parse.factor().indentedToString(0);
        assertEquals(results,expResults);
        System.out.println("\n***RESULTS***\n" + results);

        //not factor option
        System.out.println("\n~~~~not~~~~");
        sample = "not sample[this+that-5]";
        expResults="Operation: NOT\n" +
                "|-- Name: sample\n" +
                "|-- Position: \n" +
                "|-- --- Operation: PLUS\n" +
                "|-- --- --- Name: this\n" +
                "|-- --- --- Operation: MINUS\n" +
                "|-- --- --- --- Name: that\n" +
                "|-- --- --- --- Value: 5\n";
        parse = new Parser(sample,false);
        results=parse.factor().indentedToString(0);
        assertEquals(results, expResults);
        System.out.println("\n***RESULTS***\n" + results);
        }
        */
}