package codegenerator;

import symboltable.SymbolTable;
import syntaxtree.*;
import scanner.TokenType;
import java.util.ArrayList;

/**
 * This Class takes in the SyntaxTree and turns it into MIPS assembly to be written to file.
 * It can handle functions
 * @author Greg
 * created on 4/6/2017.
 */
public class GenerationController {
    private StringBuilder str;          //The main string containing all the assembly
    private ProgramNode tree;           //The SyntaxTree
    private SymbolTable symbolTable;    //SymbolTable
    private int currentRegisterS;       //Current S register available.
    private int loopNum;                //current unique loop number
    private int ifNum;                  //current unique if number

    /**
     * Constructor that accepts two arguments
     * @param tree ProgramNode for the SyntaxTree
     * @param symbolTable To get information about the variables
     */
    public GenerationController(ProgramNode tree, SymbolTable symbolTable){
        str = new StringBuilder();
        this.tree = tree;
        this.symbolTable=symbolTable;
        currentRegisterS=0;
        loopNum = 0;
        ifNum = 0;
    }

    /**
     * Generates the Assembly file. It iterates through the ProgramNode and creates assembly code
     * @return The assembly code of the ProgramNode
     */
    public String generate(){
        //generate the .data tag
        str.append(".data\n");
        //assigning the globals variables in the data section
        str.append("#SystemVariables\n");
        //adding a newline
        str.append("__newline__: .asciiz \"\\n\"\n");
        //adding an option for input
        str.append("__input__: .asciiz \"input: \"\n");
        str.append("#Global declarations\n");
        for(VariableNode var: tree.getVariables().getVars()){
            str.append(declareGlobalVariable(var));
            //set memory address
            symbolTable.setMemoryAddress(var.getName(),var.getName());
        }

        //main -- setting up
        str.append("\n.text\n");
        str.append("\n#++++++\n#Main\n#------\n");
        str.append("main:\n");
        str.append(pushStack("",0));

        //set up initial values
        for(StatementNode state:tree.getMain().getStatements()){
            String reg = "$s"+currentRegisterS++;
            str.append(writeStatement(state, reg));
            currentRegisterS--;
        }

        //ending main
        str.append(popStack("",0));
        str.append("jr $ra\n\n");

        //writing all functions
        for(SubProgramNode sub: tree.getFunctions().getProcs()){
            str.append(writeFunction(sub));
        }

        return str.toString();
    }

    /**
     * It stores the register passed in to the memory address passed in.
     * @param var The memory address of the variable
     * @param reg The register to be saved to memory
     * @return a string of the assembly code for writing to memory
     */
    private String writeToMemory(String var, String reg){
        StringBuilder str = new StringBuilder();
        str.append(String.format("#Storing register: %s to %s\n",reg,var.toUpperCase()));
        str.append(String.format("sw %s, %s\n",reg,var));
        return str.toString();
    }

    /**
     * Displays whatever is passed in to console.
     * It will also display a new line after each time it's called
     * @param state the WriteNode that contains the element to be displayed
     * @param reg the current available register
     * @return a string of assembly code containing a write to console
     */
    private String display(WriteNode state, String reg){
        StringBuilder str = new StringBuilder();
        str.append("\n#display\n");
        str.append(writeExpression(state.getExpNode(),reg));
        str.append("li $v0, 1");
        str.append(String.format("\nmove $a0, %s\n",reg));
        str.append("syscall\n");

        //print a new line after the expression
        str.append("#Displaying a new line\n");
        str.append("li $v0, 4");
        str.append("\nla $a0, __newline__\n");
        str.append("syscall\n");
        return str.toString();
    }

    /**
     * Writes the assembly code for reading in a number.
     * @param read The readNode containing the element to save the result to
     * @return a string containing the assembly code for reading from console
     */
    private String read(ReadNode read){
        StringBuilder str = new StringBuilder();
        str.append(String.format("#Reading %s\n",read.getVar().getName()));

        //adding a prompt for the user to input
        str.append("#Displaying a new line\n");
        str.append("li $v0, 4");
        str.append("\nla $a0, __input__\n");
        str.append("syscall\n");

        //determining what type the var is
        TokenType returnType = symbolTable.getReturnType(read.getVar().getName());

        if(returnType == TokenType.INTEGER){
            str.append("li $v0, 5\n");
            str.append("syscall\n");
            str.append("#Storing the input to the proper memory address\n");
            str.append(String.format("sw $v0, %s\n",symbolTable.getMemoryAddress(read.getVar().getName())));
        }
        //else it's a real
        else{
            //future implementation
        }
        return str.toString();
    }

    /**
     * declares global variables into the .data section of the assembly file
     * It will write standard variables, and array's to the .data section
     * @param var The variable node to write to the .data section
     * @return A string of assembly code to go in the .data section
     */
    private String declareGlobalVariable(VariableNode var){
        StringBuilder str = new StringBuilder();
        int length = symbolTable.getArraySize(var.getName());

        if(length == 0){
            length = 1;
        }

        //adding the variable name
        str.append(var.getName());
        str.append(": .word ");

        //adding the proper number of zeros
        for(int index = 0; index < length; index++){
            str.append("0");
            if((index+1) != length){
                str.append(", ");
            }
        }
        //currentRegisterS++;
        str.append("\n");
        return str.toString();
    }


    /**
     * offsets the $SP. Pushes all 8 of the $S registers to the stack. It will also push
     * any set register set name and number of elements passed in.
     * It also pushes the $ra and $fp to the stack
     * @param register The potential register to have it's elements saved
     * @param numItems The number of items to push
     * @return a string of assembly code containing pushing to the stack
     */
    private String pushStack(String register, int numItems){
        StringBuilder str = new StringBuilder();
        int totalNum = (numItems*4)+40;
        str.append("\n#Pushing to the stack\n");
        //stack counter
        str.append("addi $sp, $sp, -");
        str.append(totalNum);
        str.append('\n');

        //push the return address on the stack
        str.append("sw $ra, 0($sp)\n");
        str.append("sw $fp, 4($sp)\n");

        //pushing all the $s registers to the stack
        int offset = 4;
        for(int index = 0; index < 8; index++){
            offset +=4;
            str.append(String.format("sw $s%d, %d($sp)\n",index, offset));
        }

        //pushing items to the stack if needed
        if(!register.equals("")){
            for(int index = 0; index < numItems; index++){
                offset+=4;
                String s = String.format("sw %s%d, %d($sp)\n", register, index, offset);
                str.append(s);
            }

        }

        return str.toString();
    }

    /**
     * pops all 8 of the $S registers from the stack. It will also pop
     * any register set name and number of elements passed in.
     * It also pops the $ra and $fp from the stack
     * It then pops the stack to it's initial position
     * @param register The potential register to have it's elements saved
     * @param numItems The number of items to push
     * @return a string of assembly code containing popping and restoring to the stack
     */
    private String popStack(String register, int numItems){
        StringBuilder str = new StringBuilder();
        str.append("\n#Popping the stack\n");

        int totalNum = (numItems*4)+40;

        //restoring the $fp and $ra
        str.append("lw $ra, 0($sp)\n");
        str.append("lw $fp, 4($sp)\n");

        int offset = 4;
        //popping off the $S
       for(int index = 0; index < 8; index++){
           offset +=4;
           String s = String.format("lw $s%d, %d($sp)\n", index, offset);
           str.append(s);
       }

        //pushing items to the stack if needed
        if(!register.equals("")){
            for(int index = 0; index < numItems; index++){
                offset+=4;
                String s = String.format("lw %s%d, %d($sp)\n", register, index, offset);
                str.append(s);
            }
        }

        //popping the stack
        str.append("addi $sp, $sp, ");
        str.append(totalNum);
        str.append("\n");

        return str.toString();
    }

    /**
     * Writes statements in assembly
     * @param state The statement to be written
     * @param reg The reg to assign the statement to
     * @return A string containing the assembly commands
     */
    private String writeStatement(StatementNode state, String reg) {
        StringBuilder str = new StringBuilder();
        switch (state.getClass().getSimpleName()) {
            case "AssignmentStatementNode":
                str.append(writeAssignment((AssignmentStatementNode) state, reg));
                break;
            case "CompoundStatementNode":
                for (StatementNode sn : ((CompoundStatementNode) state).getStatements()) {
                    //String register = "$s"+currentRegisterS++;
                    str.append(writeStatement(sn,reg));
                }
                //currentRegisterS -= ((CompoundStatementNode)state).getStatements().size();
                break;
            case "IfStatementNode":
                str.append(writeIf((IfStatementNode) state,reg));
                break;
            case "ReadNode":
                str.append(read((ReadNode)state));
                break;
            case "ProcedureStatementNode":
                str.append(writeProcedure((ProcedureStatementNode)state));
                break;
            case "WhileStatementNode":
                str.append(writeLoop(((WhileStatementNode)state), reg));
                break;
            case "WriteNode":
                str.append(display(((WriteNode) state),reg));
                break;
        }
        return str.toString();
    }

    /**
     * Writes the assembly need to call a procedure.
     * @param state the ProcedureStatementNode
     * @return The code containing the elements needed to call a procedure
     */
    private String writeProcedure(ProcedureStatementNode state){
        StringBuilder str = new StringBuilder();
        str.append("#Procedure Call\n");

        //storing the arguments into the argument registers
        int argumentNumber = 0;
        for(ExpressionNode exp: state.getExpNode()){
            //current register
            String register="$a"+ argumentNumber++;
            str.append(writeExpression(exp,register));
        }
        //calling the function
        str.append(String.format("jal %s\n",state.getVariable()));

        return str.toString();
    }

    /**
     * Writes the code needed for an assignment.
     * It will first evaluate the expression of the assignment,
     * and then assign it to the register passed in
     * @param node The AssignmentStatementNode of the assignment
     * @param reg The register to have the expression assigned to
     * @return A string containing the code needed for an AssignmentStatement in assembly
     */
    private String writeAssignment(AssignmentStatementNode node, String reg){
        StringBuilder str = new StringBuilder();
        str.append("\n#Assignment\n");
        //evaluating the expression
        str.append(writeExpression(node.getExpression(),reg));
        String varName =  node.getLvalue().getName();
        //storing to memory
        str.append(writeToMemory(symbolTable.getMemoryAddress(varName),reg));

        return str.toString();
    }

    /**
     * Writes the code needed to evaluate an expression.
     * It will switch through the different types of expression nodes.
     * Each type of node has a different type of code needed to accomplish it
     * @param exp The Expression to have assembly written for it
     * @param reg The register to have it's value saved to
     * @return A string containing the Expressions code in assembly
     */
    private String writeExpression(ExpressionNode exp, String reg){
        StringBuilder str = new StringBuilder();

        switch(exp.getClass().getSimpleName()){
            case "ValueNode":
                str.append(String.format("li %s, %s\n",reg, ((ValueNode)exp).getAttribute()));
                break;
            case "OperationNode":
                str.append(writeOperation((OperationNode)exp,(reg)));
                break;
            case "VariableNode":
                //get the value at the variable node
                String var = symbolTable.getMemoryAddress(((VariableNode)exp).getName());
                str.append("lw "+reg+", "+var+"\n");
                break;
            case "FunctionNode":
                //push items to the stack pointer
                str.append("#FunctionCall\n");
                str.append("#Putting variables in the $a registers\n");
                int argNum = ((FunctionNode)exp).getExpNode().size();
                for(int index = 0; index < argNum; index++){
                    String register = "$a"+index;
                    ExpressionNode arg = ((FunctionNode)exp).getExpNode().get(index);
                    str.append(writeExpression(arg,register));
                }

                //calling the function
                str.append(String.format("jal %s\n",((FunctionNode)exp).getName()));

                //return statement
                str.append(String.format("add %s, $v0, $zero\n",reg));

                break;
                //TODO implement ARRAYS
            case "ArrayNode":
                str.append(writeExpression(((ArrayNode)exp).getExpNode(),reg));
                break;
        }
        return str.toString();
    }

    /**
     * writes the OperationNode in assembly.
     * It will assign a register to the left and right part of the Operation.
     * It will then recursively call it's self until the left and right nodes are
     * either value nodes or variable nodes. Once it is at that state it will switch based
     * what type of operation is happening. When it unfolds it will build the proper sequence of expressions
     * based on what the user has inserted.
     * @param node The operationNode that is needed to write assembly for it.
     * @param result Register to have the operation saved to
     * @return A string containing the assembly code for an operationNode
     */
    private String writeOperation(OperationNode node, String result){
        StringBuilder str = new StringBuilder();
        ExpressionNode left = node.getLeft();
        String leftRegister = "$s" + currentRegisterS++;
        str.append(writeExpression( left, leftRegister));
        ExpressionNode right = node.getRight();
        String rightRegister = "$s" + currentRegisterS++;
        str.append(writeExpression( right, rightRegister));
        switch(node.getOperation()){
            case PLUS:
                str.append(String.format("add %s, %s, %s \n",result,leftRegister,rightRegister));
                break;
            case MINUS:
                str.append(String.format("sub %s, %s, %s \n",result,leftRegister,rightRegister));
                break;
            case TIMES:
                str.append(String.format("mult %s, %s \n",leftRegister,rightRegister));
                str.append(String.format("mflo %s\n",result));
                break;
                //TODO implement DIVIDE => Floating point
            case DIVIDE:
            case DIV:
                str.append(String.format("div %s, %s\n",leftRegister,rightRegister));
                str.append(String.format("mflo %s\n",result));
                break;
            case MOD:
                str.append(String.format("div %s, %s\n",leftRegister,rightRegister));
                str.append(String.format("mfhi %s\n",result));
                break;
            case OR:
                str.append(String.format("or %s, %s, %s\n",result,leftRegister,rightRegister));
                break;
            case AND:
                str.append(String.format("and %s, %s, %s\n",result,leftRegister,rightRegister));
                break;
            case LESS_THAN:
                //branch left >= right
                str.append(String.format("bge %s, %s, ",leftRegister,rightRegister));
                break;
            case LESS_THAN_EQUAL_TO:
                //branch left > right
                str.append(String.format("bgt %s, %s, ",leftRegister,rightRegister));
                break;
            case GREATER_THAN:
                //branch left <= right
                str.append(String.format("ble %s, %s, ",leftRegister,rightRegister));
                break;
            case GREATER_THAN_EQUAL_TO:
                //branch left < right
                str.append(String.format("blt %s, %s, ",leftRegister,rightRegister));
                break;
            case EQUALITY_OPERATOR:
                //branch left != right
                str.append(String.format("bne %s, %s, ",leftRegister,rightRegister));
                break;
            case NOT_EQUAL:
                //branch left == right
                str.append(String.format("beq %s, %s, ",leftRegister,rightRegister));
                break;
        }

        this.currentRegisterS -= 2;
        return str.toString();
    }

    /**
     * Writes the assembly needed to implement a loop in assembly.
     * It will name each loop with a unique identifier inorder to clear ambiguity.
     * @param node The whileStatementNode to be written
     * @param reg The register to save the result to
     * @return A string containing the assembly code for a While Statement
     */
    private String writeLoop(WhileStatementNode node, String reg){
        StringBuilder str = new StringBuilder();
        int number = loopNum++;

        str.append("\n#Loop\n");
        //start of the loop
        str.append("loop"+number+":\n");

        //write the expression
        str.append(writeExpression(node.getTest(),reg));
        str.append("endLoop"+number+"\n");
        //write the statements
        String stateReg = "$s"+currentRegisterS++;
        str.append(writeStatement(node.getDoStatement(),stateReg));

        //jump back to the top
        str.append("j loop"+number+"\n");

        //end of the loop
        str.append("endLoop"+number+":\n");

        currentRegisterS -= 1;
        return str.toString();
    }

    /**
     * Writes the code needed to execute an If statement
     * Each if statement will have a unique identifier assigned to it
     * inorder to stop ambiguity
     * @param node The IF statementNode to have it's code written
     * @param reg The register to save the result to
     * @return a string that represents the code needed to evaluate an if statement in Assembly
     */
    private String writeIf(IfStatementNode node, String reg){
        StringBuilder str = new StringBuilder();
        int number = ifNum++;
        str.append("\n#If statement\n");
        //calculate the expression

        str.append(writeOperation((OperationNode) node.getTest(),reg));
        str.append("else"+number+"\n");


        //write then
        str.append("#Then\n");
        String then = "$s"+currentRegisterS++;
        str.append(writeStatement(node.getThenStatement(),then));
        //jump to end
        str.append("j endIf"+number+"\n");

        //write else
        str.append("#Else\n");
        String Else = "$s"+currentRegisterS++;
        str.append("else"+number+":\n");
        str.append(writeStatement(node.getElseStatement(),Else));

        //display the end
        str.append("endIf"+number+":\n");

        currentRegisterS-=2;
        return str.toString();
    }

    /**
     * Takes in the number of arguments, adds them to the symbol table,
     * then assigns a frame pointer to them as their memory address
     * @param fctn the SubProgramNode to have it's local variables memory addressed
     * @return A string containing the code needed to assign local variables to memory
     */
    private String AssignMemoryAddress(SubProgramNode fctn){
        int localVars = fctn.getVariables().getVars().size();
        int argNum = fctn.getArguments().size();
        int offset = 0;
        ArrayList<VariableNode> vars = fctn.getVariables().getVars();
        for(int index = 0; index < localVars; index++){
            String register = offset + "($sp)";
            symbolTable.setMemoryAddress(vars.get(index).getName(), register);
            offset += 4;

        }

        StringBuilder str = new StringBuilder();
        str.append("#saving $a to their proper pointer position\n");
        vars = fctn.getArguments();
        //assigning the arguments
        //storing the arguments to their new memory location
        for(int index = 0; index < argNum; index++){
            String register = offset+"($sp)";
            symbolTable.setMemoryAddress(vars.get(index).getName(),register);
            str.append(String.format("sw $a%d, %s\n",index,register));

            offset+=4;
        }
        return str.toString();
    }

    /**
     * writes a function in mips assembly
     * It will preform similar operations as the generate function. There will
     * be a few differences. It's variables aren't saved in the .data section. They are
     * saved in an offset from the stack pointer. The frame pointer will hold the original stack pointer
     * so variables in main aren't deleted
     * @param fctn to have written in mips assembly
     * @return a string containing the mips Assembly of the function
     */
    private String writeFunction(SubProgramNode fctn){
        StringBuilder str = new StringBuilder();
        int numPushed = currentRegisterS;
        currentRegisterS=0;

        //the offset
        int offset = 0;
        if(symbolTable.getLocalTable(fctn.getName()) != null)
            offset = symbolTable.getLocalTable(fctn.getName()).size();

        //adding the local symbol table to the stack
        symbolTable.pushSymbolTable(symbolTable.getLocalTable(fctn.getName()));

        //adding the function name
        str.append("\n#++++++\n#Function\n#------\n");
        str.append(fctn.getName() + ":\n");

        //pushing the proper number of elements on the stack
        str.append(pushStack("",0));

         //Assign variables to their memory address
        str.append("\n#Moving fp and sp\n");
        str.append("move $fp, $sp\n");

        //fixing the stack for local stacks
        str.append("\n#pushing the stack\n");
        str.append(String.format("addi $sp, $sp, -%d\n",(offset*4)));

        //assigning value
        str.append(AssignMemoryAddress(fctn));
        for(StatementNode state: fctn.getMain().getStatements()){
            String reg = "$s"+currentRegisterS;
            str.append(writeStatement(state, reg));
        }

        //setting the return value if applicable
        if(fctn.getIsFunction()){
            str.append("#Setting the return value\n");
            str.append(String.format("lw $v0, %s\n", symbolTable.getMemoryAddress(fctn.getName())));
        }

        //popping the local stack
        str.append("\n#popping the stack\n");
        str.append(String.format("addi $sp, $sp, %d\n",(offset*4)));

        //restoring the stack pointer
        str.append("\n#Moving sp and fp\n");
        str.append("move $sp, $fp\n");

        //popping the stack back out
        str.append(popStack("",0));

        //jump and return
        str.append("jr $ra\n");

        //subprograms
        for(SubProgramNode subProgramNode: fctn.getFunctions().getProcs()){
            str.append(writeFunction(subProgramNode));
        }

        //restoring the currentRegisterS count
        currentRegisterS = numPushed;
        symbolTable.removeScope();
        return str.toString();
    }

}
