package syntaxtree;

import scanner.TokenType;

/**
 * Represents any operation in an expression.
 * @author Erik Steinmetz
 */
public class OperationNode extends ExpressionNode {
    
    /** The left operator of this operation. */
    private ExpressionNode left;
    
    /** The right operator of this operation. */
    private ExpressionNode right;
    
    /** The kind of operation. */
    private TokenType operation;
    
    /**
     * Creates an operation node given an operation token.
     * @param op The token representing this node's math operation.
     */
    public OperationNode ( TokenType op) {
        this.operation = op;
    }


    /**
     * Returns the Left ExpressionNode of the Operation
     * @return the Left ExpressionNode of the Operation
     */
    public ExpressionNode getLeft() { return( this.left);}

    /**
     * Returns the right ExpressionNode of the Operation
     * @return the right ExpressionNode of the Operation
     */
    public ExpressionNode getRight() { return( this.right);}

    /**
     * Gets the operation
     * @return the operation of the node
     */
    public TokenType getOperation() { return( this.operation);}

    /**
     * Sets the left ExpressionNOde of the Operation.
     * Determines and sets the dataType of the expression
     * @param node to be set to the left expression
     */
    public void setLeft( ExpressionNode node) {
        // If we already have a left, remove it from our child list.
        this.left = node;

        if(super.dataType==null){
            super.dataType=node.dataType;
        }
        else if(super.dataType == TokenType.INTEGER && node.dataType==TokenType.REAL){
            super.dataType=TokenType.REAL;
        }
    }
    /**
     * Sets the right ExpressionNOde of the Operation.
     * Determines and sets the dataType of the expression
     * @param node to be set to the left expression
     */
    public void setRight( ExpressionNode node) { 
        // If we already have a right, remove it from our child list.
        this.right = node;


        if(super.dataType==null){
            super.dataType=node.dataType;
        }
        else if(super.dataType == TokenType.INTEGER && node.dataType==TokenType.REAL){
            super.dataType=TokenType.REAL;
        }
    }
    
    /**
     * Returns the operation token as a String.
     * @return The String version of the operation token.
     */
    @Override
    public String toString() {
        return operation.toString();
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Operation: " + this.operation +  " (" + this.dataType +")"+"\n";
        answer += left.indentedToString(level + 1);
        answer += right.indentedToString(level + 1);
        return( answer);
    }


    /**
     * Checks if another OperationNode is equal to this one
     * @param o an object to be compared to
     * @return True if the objects are equal. False if they are not equal.
     */
    @Override
    public boolean equals( Object o) {
        boolean answer = false;
        if( o instanceof OperationNode) {
            OperationNode other = (OperationNode)o;
            if( (this.operation == other.operation) &&
                    this.left.equals( other.left) &&
                    this.right.equals( other.right)) answer = true;
        }
        return answer;
    }
}
