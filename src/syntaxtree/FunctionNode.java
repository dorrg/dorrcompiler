package syntaxtree;

import scanner.TokenType;
import java.util.ArrayList;

/**
 * FunctionNode is used when a program is trying to call a function. It extends VariableNode which extends ExpressionNode
 * @author Greg
 * 3/15/2017.
 */
public class FunctionNode extends VariableNode{
    /** List of arguments being passed into a function */
    private ArrayList<ExpressionNode> expNode;


    /**
     * Creates an ArrayNode and the parent variableNode with the given attribute.
     * @param attr The attribute for this value node.
     * @param dataType of the expression
     */
    public FunctionNode( String attr, TokenType dataType) {
        super(attr, dataType);
        expNode = new ArrayList<>();
    }

    /**
     * Returns the name of the variable of this node.
     * @return The name of this VariableNode.
     */
    public String getName() { return( super.name);}

    /**
     * sets the expression list
     * @return an ArrayList of ExpressionNodes
     */
    public ArrayList<ExpressionNode> getExpNode(){return this.expNode;}

    /**
     * Sets the ArrayList of expressionNodes
     * @param input of expressionNodes to be added
     */
    public void setExpNode(ArrayList<ExpressionNode> input){
        this.expNode = input;
    }

    /**
     * Returns the name of the variable as the description of this node.
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return( "VariableNode: " + super.name + "ExpressionNode: " + expNode);
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        StringBuilder answer = new StringBuilder(this.indentation(level));
        answer.append("Name: ").append(super.name).append("\n");
        answer.append(this.indentation(level));
        answer.append("Arguments: \n");
        for( ExpressionNode expression : expNode) {
            answer.append(expression.indentedToString(level + 1));
        }
        return answer.toString();
    }

    /**
     * Checks if another FunctionNode is equal to this one
     * @param o an object to be compared to
     * @return True if the objects are equal. False if they are not equal.
     */
    @Override
    public boolean equals( Object o) {
        boolean answer = false;
        if( o instanceof FunctionNode) {
            FunctionNode other = (FunctionNode)o;
            if( super.name.equals( other.getName()) && ( this.expNode.equals(other.getExpNode())))
                answer = true;
        }
        return answer;
    }
}
