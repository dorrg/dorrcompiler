package syntaxtree;

import java.util.ArrayList;

/**
 * ProcedureStatementNode is used to call a procedure.
 * @author Greg Dorr
 * 3/13/2017.
 */
public class ProcedureStatementNode extends StatementNode {

    private String name;                        //The Procedure's variable
    private ArrayList<ExpressionNode> expNode;  //Arguments

    /**
     * Constructor that takes in no arguments
     */
    public ProcedureStatementNode(){
        name = null;
        expNode = new ArrayList();
    }

    /**
     * Sets the variable node
     * @param input sets the variable node
     */
    public void setVariable(String input){this.name = input;}

    /**
     * Sets the arguments for the procedure call
     * @param input input to be set as procedure arguments
     */
    public void setExpNode(ArrayList<ExpressionNode> input){this.expNode = input;}

    /**
     * Sets the arguments
     * @param input sets the variable node
     */
    public void addAllExpNode(ArrayList<ExpressionNode> input){expNode.addAll(input);}

    /**
     * returns the variable node
     * @return the variable node
     */
    public String getVariable(){return this.name;}

    /**
     * returns all the arguments
     * @return the arguments
     */
    public ArrayList<ExpressionNode> getExpNode(){return expNode;}


    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        StringBuilder answer = new StringBuilder(this.indentation(level));
        answer.append("ProcedureName: ").append(this.name).append("\n");
        for( ExpressionNode exp : expNode) {
            answer.append(exp.indentedToString(level + 1));
        }
        return answer.toString();
    }
}
