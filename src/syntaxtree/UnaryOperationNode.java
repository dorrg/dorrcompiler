package syntaxtree;

import scanner.TokenType;

/**
 * UnaryOperationNode holds a positive, negative, or not operation
 * It also holds a an expression.
 * @author Greg
 * created on 3/13/2017.
 */
public class UnaryOperationNode extends ExpressionNode {

    /** The right operator of this operation. */
    private ExpressionNode expression;

    /** The kind of operation. */
    private TokenType operation;

    /**
     * Creates an operation node given an operation token.
     * @param op The token representing this node's math operation.
     */
    public UnaryOperationNode ( TokenType op) {
        this.operation = op;
    }

    /**
     * Retuns the expression
     * @return the expression
     */
    public ExpressionNode getExpression() { return( this.expression);}

    /**
     * Returns the operation of the node, it can be: PLUS, MINUS, or NOT
     * @return The operation of the node
     */
    public TokenType getOperation() { return( this.operation);}

    /**
     * Sets the expression to what's passed in. It will also figure out the data type based
     * on the data type being passed in. it will Automatically upgrade if needed.
     * @param node To be set as the expression
     */
    public void setExpression( ExpressionNode node) {
        // If we already have a left, remove it from our child list.
        this.expression = node;

        if(super.dataType==null){
            super.dataType=node.dataType;
        }
        else if(super.dataType == TokenType.INTEGER && node.dataType==TokenType.REAL){
            super.dataType=TokenType.REAL;
        }

    }

    /**
     * Returns the operation token as a String.
     * @return The String version of the operation token.
     */
    @Override
    public String toString() {
        return operation.toString();
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "UnaryOp: " + this.operation + "\n";
        answer += expression.indentedToString(level + 1);
        return( answer);
    }

    /**
     * Compares two UnaryOperationNode to check if they are equal.
     * @param o to be compared
     * @return True if they are equal. False otherwise
     */
    @Override
    public boolean equals( Object o) {
        boolean answer = false;
        if( o instanceof syntaxtree.UnaryOperationNode) {
            syntaxtree.UnaryOperationNode other = (syntaxtree.UnaryOperationNode)o;
            if( (this.operation == other.operation) && (this.expression.equals( other.expression)))
                     answer = true;
        }
        return answer;
    }
}

