package syntaxtree;

/**
 * WhileStatementNode holds the information need to create a while statement in assembly.
 * It has a expression for the condition or test and a statement or body of the loop
 * @author Greg
 * writen on 3/4/2017.
 */
public class WhileStatementNode extends StatementNode {

    private ExpressionNode test;        //condition thats tested each iteration of the loop
    private StatementNode doStatement;  //Statement that executes when the test is true

    /**
     * Constructor that accepts no arguments
     * and defaults all values to null
     */
    public WhileStatementNode(){
        test = null;
        doStatement = null;
    }

    /**
     * Returns the test
     * @return the test condition
     */
    public ExpressionNode getTest() {
        return test;
    }

    /**
     * Sets the Test condition that the loop check each iteration
     * @param test To be set as the test condition
     */
    public void setTest(ExpressionNode test) {
        this.test = test;
    }

    /**
     * Returns the Statement that executes when this test is true
     * @return the body statement
     */
    public StatementNode getDoStatement() {return doStatement;}

    /**
     * Sets the statement to execute when the test is true
     * @param doStatement to be set as the test
     */
    public void setDoStatement(StatementNode doStatement) {
        this.doStatement = doStatement;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "While\n";
        answer += this.test.indentedToString( level + 1);
        answer += this.indentation( level)+"Do\n";
        answer += this.doStatement.indentedToString( level + 1);
        return answer;
    }
}
