package syntaxtree;

import scanner.TokenType;

/**
 * Represents a variable in the syntax tree.
 * @author Erik Steinmetz
 */
public class VariableNode extends ExpressionNode {
    
    /** The name of the variable associated with this node. */
    protected String name;

    /**
     * Creates a ValueNode with the given attribute.
     * @param attr The attribute for this value node.
     */
    public VariableNode( String attr) {
        super(null);
        this.name = attr;
    }

    /**
     * Creates a VariableNode with the given attribute.
     * @param attr The attribute for this value node.
     * @param dataType The data Type of the object
     */
    public VariableNode( String attr, TokenType dataType) {
        super(dataType);
        this.name = attr;
    }

    /** 
     * Returns the name of the variable of this node.
     * @return The name of this VariableNode.
     */
    public String getName() { return( this.name);}

    @Override
    public String toString() {
        return( name);
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Name: " + this.name;
        if(super.dataType!=null){
            answer+= " (" + super.dataType + ")";
        }
        answer += "\n";
        return answer;
    }

    /**
     * Compares two VariableNodes to check if they are equal.
     * @param o to be compared
     * @return true if they are equal. false otherwise
     */
    @Override
    public boolean equals( Object o) {
        boolean answer = false;
        if( o instanceof VariableNode) {
            VariableNode other = (VariableNode)o;
            if( (this.name.equals( other.name)) && (other.getDataType() == super.dataType)) answer = true;
        }
        return answer;
    }    
    
}
