
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a set of declarations in a Pascal program.
 * @author Erik Steinmetz
 */
public class DeclarationsNode extends SyntaxTreeNode {

    private ArrayList<VariableNode> vars;   //holds the variable nodes

    /**
     * Default constructor that accepts no arguments
     */
    public DeclarationsNode(){
        vars = new ArrayList<>();
    }

    /**
     * Adds a single variable node to the list of VariableNodes
     * @param aVariable to be added to the list of VariableNodes
     */
    public void addVariable( VariableNode aVariable) {
        vars.add( aVariable);
    }

    /**
     * Adds a whole list of VariableNodes to this list
     * @param decNode to be added to the list of VariableNodes
     */
    public void addDeclarations( DeclarationsNode decNode){
        vars.addAll(decNode.vars);
    }

    /**
     * Returns the Variables
     * @return ArrayList of VariableNodes
     */
    public ArrayList<VariableNode> getVars(){
        return this.vars;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    public String indentedToString( int level) {
        StringBuilder answer = new StringBuilder(this.indentation(level));
        answer.append("Declarations\n");
        for( VariableNode variable : vars) {
            answer.append(variable.indentedToString(level + 1));
        }
        return answer.toString();
    }

}
