package syntaxtree;

import scanner.TokenType;

/**
 * General representation of any expression.
 * @author erik
 */
public abstract class ExpressionNode extends SyntaxTreeNode {
    protected TokenType dataType;

    /**
     * constructor the accepts no arguments
     */
    public ExpressionNode(){
        this.dataType=null;
    }

    /**
     * @param dataType type of expression
     * constructor the accepts a type
     */
    public ExpressionNode(TokenType dataType){
        this.dataType=dataType;
    }

    /**
     * sets the dataType of the expression
     * @param input Token type of the expression
     */
    public void setDataType(TokenType input){
        this.dataType=input;
    }

    /**
     * Gets the Data type of the expression
     * @return the data type of the expression
     */
    public TokenType getDataType(){return this.dataType;}
}


