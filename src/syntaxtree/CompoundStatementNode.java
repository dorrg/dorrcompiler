
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a compound statement in Pascal.
 * A compound statement is a block of zero or more
 * statements to be run sequentially.
 * @author ErikSteinmetz
 */
public class CompoundStatementNode extends StatementNode {

    private ArrayList<StatementNode> statements;    //holds the statements

    /**
     * Default Constructor
     */
    public CompoundStatementNode(){
        statements=new ArrayList<>();
    }

    /**
     * Adds a single statement the the statement List
     * @param state to be added to the Statement List
     */
    public void addStatement( StatementNode state) {
        this.statements.add( state);
    }

    /**
     * Adds multiple StatementNodes to the central StatementNode in this class
     * @param csNode To be added to the central StatementNode of this class
     */
    public void addAllStatements(ArrayList<StatementNode> csNode){this.statements.addAll(csNode);}

    /**
     * Returns the list of the statements
     * @return ArrayList
     */
    public ArrayList<StatementNode> getStatements(){
        return statements;
    }
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    public String indentedToString( int level) {
        StringBuilder answer = new StringBuilder(this.indentation(level));
        answer.append("Compound Statement\n");
        for( StatementNode state : statements) {
            answer.append(state.indentedToString(level + 1));
        }
        return answer.toString();
    }
}
