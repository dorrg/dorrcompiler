package syntaxtree;

import scanner.TokenType;

/**
 * ArrayNode is used to access an array at a certain position. It extends VariableNode and ExpressionNode
 * @author Greg Dorr
 * 3/14/2017.
 */
public class ArrayNode extends VariableNode{

    private ExpressionNode expNode;     //Expression Node for the position of the array

    /**
     * Creates an ArrayNode and the parent variableNode with the given attribute.
     * @param attr The attribute for this value node.
     * @param tt TokenType of the arrayNode.
     */
    public ArrayNode( String attr,TokenType tt) {
        super(attr);
        super.setDataType(tt);
        expNode = null;

    }

    /**
     * Returns the name of the variable of this node.
     * @return The name of this VariableNode.
     */
    public String getName() { return( super.name);}

    /**
     * returns the postion of the array that is wanting to be accessed.
     * @return The Expression Node that contains the position of the array
     */
    public ExpressionNode getExpNode(){return this.expNode;}

    /**
     * Sets the postion of the Array
     * @param input for the position of the array to be accessed
     */
    public void setExpNode(ExpressionNode input){
        this.expNode = input;
    }

    /**
     * Returns the name of the variable as the description of this node.
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return( "VariableNode: " + super.name + "ExpressionNode: " + expNode);
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Name: " + super.name + " (" + dataType + ")"+ "\n";
        answer += this.indentation(level) + "Position: \n" + this.expNode.indentedToString(level +1);
        return answer;
    }

    /**
     * Compares two ArrayNodes to check if they are equal.
     * @param o to be compared
     * @return true if they are equal. false if they aren't
     */
    @Override
    public boolean equals( Object o) {
        boolean answer = false;
        if( o instanceof ArrayNode) {
            ArrayNode other = (ArrayNode)o;
            if( super.name.equals( other.getName()) && ( this.expNode.equals(other.getExpNode())))
                answer = true;
        }
        return answer;
    }
}
