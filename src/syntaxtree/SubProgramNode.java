package syntaxtree;

import scanner.TokenType;
import java.util.ArrayList;

/**
 * The SubProgramNode is used for functions, procedures or nested programs.
 * It holds: the name of the operation, any variables declared, any nested SubProgramNodes,
 * the main body of the operation, the return type (if any), and an array list of arguments (if any).
 *
 * @author Greg Dorr
 */
public class SubProgramNode extends SyntaxTreeNode{

    private String subProgramName;                              //Sub Program Name
    private DeclarationsNode subProgramVariables;               //Declarations Node
    private SubProgramDeclarationsNode subProgramFunctions;     //SubProgram Declarations Node
    private CompoundStatementNode subProgramMain;               //Compound Statement Node
    private TokenType returnType = null;                        //Return type of the expression
    private ArrayList<VariableNode> arguments;                  //Arguments for the function
    private boolean isFunction;

    /**
     * Constructor that accepts only one parameter; The name of the node.
     * @param aName of the SubProgramNode
     * @param isFunction if it's a function or not
     */
    public SubProgramNode( String aName, boolean isFunction) {
        this.subProgramName = aName;
        returnType = null;
        arguments = new ArrayList<>();
        this.isFunction=isFunction;
    }

    /**
     * returns the name of the function or procedure
     * @return the name of the function or procedure
     */
    public String getName(){
        return this.subProgramName;
    }

    /**
     * returns a Declarations node with the variables in it
     * @return the declarations node with the subprogram in it
     */
    public DeclarationsNode getVariables() {
        return subProgramVariables;
    }

    /**
     * sets the subprogramDeclarationsNode.
     * @param variables to set as the subprogram's DeclarationsNode
     */
    public void setVariables(DeclarationsNode variables) {
        this.subProgramVariables = variables;
    }

    /**
     * Returns the list of functions and procedures
     * @return the list of functions and procedures
     */
    public SubProgramDeclarationsNode getFunctions() {
        return subProgramFunctions;
    }

    /**
     * Sets the subprogramDeclarations Node if there is one. This could be another sub function or procedure
     * @param functions to set as the SubProgramDeclarationsNode
     */
    public void setFunctions(SubProgramDeclarationsNode functions) {
        this.subProgramFunctions = functions;
    }

    /**
     * returns the CompoundStatementNode that holds the statements of the procedure/function
     * @return The main body of the subprogram
     */
    public CompoundStatementNode getMain() {
        return subProgramMain;
    }

    /**
     * This sets the main body of the function or procedure
     * @param main to set as the main body of the function or procedure
     */
    public void setMain(CompoundStatementNode main) {
        this.subProgramMain = main;
    }

    /**
     * Sets the return type of the operation (if any).
     * @param type of the operation. Either Integer or Real.
     */
    public void setReturnType(TokenType type){this.returnType=type;}

    /**
     * <strong>Functions only</strong> returns the return type of the function
     * @return the return type
     */
    public TokenType getReturnType(){return this.returnType;}

    /**
     * Sets the arguments for the function or procedure
     * @param var a list of VariableNodes to set as the arguments
     */
    public void addAllArguments(ArrayList<VariableNode> var){arguments.addAll(var);}

    /**
     * Adds a single argument to the list of arguments
     * @param var The variable to be added to the list of arguments
     */
    public void addArgument(VariableNode var){arguments.add(var);}
    /**
     * returns the list of arguments
     * @return the list of arguments
     */
    public ArrayList<VariableNode> getArguments(){
        return arguments;
    }

    /**
     * Returns if it's a functino or not
     * @return if it's a functino or not
     */
    public boolean getIsFunction(){
        return this.isFunction;
    }


    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        StringBuilder answer = new StringBuilder(this.indentation( level));
        answer.append("SubProgramName: " + subProgramName);
        if(returnType!=null){
            answer.append(" (" + this.returnType+")");
        }
        answer.append("\n");
        answer.append( this.indentation(level)+"Arguments: ");
        for(VariableNode var: arguments){
            answer.append(var.getName()+"("+var.getDataType() +") ");
        }
        answer.append("\n");
        answer.append(subProgramVariables.indentedToString( level + 1));
        answer.append(subProgramFunctions.indentedToString( level + 1));
        answer.append(subProgramMain.indentedToString( level + 1));
        return answer.toString();
    }
}
