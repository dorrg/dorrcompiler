
package syntaxtree;

/**
 * Represents an if statement in Pascal.
 * An if statement includes a boolean expression, and two statements.
 * @author Erik Steinmetz
 */
public class IfStatementNode extends StatementNode {

    private ExpressionNode test;            //Expression to be evaluated for true or false
    private StatementNode thenStatement;    //Statement that executes if test is true
    private StatementNode elseStatement;    //Statement that executes if test is false.

    /**
     * Returns the test statement
     * @return The test statement
     */
    public ExpressionNode getTest() {
        return test;
    }

    /**
     * Sets the expression to be evaluated
     * @param test An expression to be evaluated
     */
    public void setTest(ExpressionNode test) {
        this.test = test;
    }

    /**
     * Returns the statement that happens if the test statement is true
     * @return Then Statement
     */
    public StatementNode getThenStatement() {
        return thenStatement;
    }

    /**
     * Sets the statement to execute if the test passes
     * @param thenStatement Statement that executes if the test passes
     */
    public void setThenStatement(StatementNode thenStatement) {
        this.thenStatement = thenStatement;
    }

    /**
     * Returns the else statement
     * @return the else statement
     */
    public StatementNode getElseStatement() {
        return elseStatement;
    }

    /**
     * Sets the statement to execute if the test fails
     * @param elseStatement Statement that executes if the test fails
     */
    public void setElseStatement(StatementNode elseStatement) {
        this.elseStatement = elseStatement;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer +=  "If\n";
        answer += this.test.indentedToString( level + 1);
        answer += this.thenStatement.indentedToString( level + 1);
        answer += this.indentation( level) + "Then\n";
        answer += this.elseStatement.indentedToString( level + 1);
        answer += this.indentation( level) + "Else\n";
        return answer;
    }

}
