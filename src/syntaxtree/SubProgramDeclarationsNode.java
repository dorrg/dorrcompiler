
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a collection of subprogram declarations
 * @author Erik Steinmetz
 */
public class SubProgramDeclarationsNode extends SyntaxTreeNode {
    
    private ArrayList<SubProgramNode> procs;    //Holds the subprograms

    /**
     * Constructor that accepts no arguments
     */
    public SubProgramDeclarationsNode(){
        procs = new ArrayList<>();
    }

    /**
     *  Sets a SubPrgramNode to the list of SubProgramNodes
     * @param aSubProgram to add to the list
     */
    public void addSubProgramDeclaration( SubProgramNode aSubProgram) {
        procs.add( aSubProgram);
    }

    /**
     * Set the whole list at once
     * @param aSubProgram to set as the procedure list
     */
    public void addall( ArrayList<SubProgramNode> aSubProgram) {
        procs.addAll( aSubProgram);
    }

    /**
     * returns the whole subProgramNode list
     * @return the whole list of SubProgramNodes
     */
    public ArrayList<SubProgramNode> getProcs(){return procs;}

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        StringBuilder answer = new StringBuilder(this.indentation(level));
        answer.append("SubProgramDeclarations\n");
        for( SubProgramNode subProg : procs) {
            answer.append(subProg.indentedToString(level + 1));
        }
        return answer.toString();
    }
    
}
