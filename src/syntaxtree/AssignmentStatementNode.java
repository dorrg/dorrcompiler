
package syntaxtree;

/**
 * Represents a single assignment statement.
 * @author Erik Steinmetz
 */
public class AssignmentStatementNode extends StatementNode {
    
    private VariableNode lvalue;        //The variable an expression is getting assigned to
    private ExpressionNode expression;  //The Expression that is being assigned

    /**
     * Returns the variableNode in the assignment
     * @return The variable Node that something is being assigned to
     */
    public VariableNode getLvalue() {
        return lvalue;
    }

    /**
     * Sets the variable node for assignment.
     * @param lvalue The Variable that an expression will be assigned to
     */
    public void setLvalue(VariableNode lvalue) {
        this.lvalue = lvalue;
    }

    /**
     * Gets the ExpressionNode of the assignment
     * @return the ExpressionNode of the assignment
     */
    public ExpressionNode getExpression() {
        return expression;
    }

    /**
     * Sets the ExpressionNode for the assignment
     * @param expression The ExpressionNode to assign to a variable
     */
    public void setExpression(ExpressionNode expression) {
        this.expression = expression;
    }


    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Assignment\n";
        answer += this.lvalue.indentedToString( level + 1);
        answer += this.expression.indentedToString( level + 1);
        return answer;
    }
}
