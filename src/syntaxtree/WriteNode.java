package syntaxtree;

/**
 * WriteNode holds all the information needed inorder to write to terminal in mips assembly
 *
 * @author Greg
 * created on 3/24/2017.
 */
public class WriteNode extends StatementNode {

    private ExpressionNode expNode; //The expression to be written to terminal

    /**
     * Sets the Expression to be written
     * @param input Expression node to set as the main expression
     */
    public void setExpNode(ExpressionNode input){this.expNode=input;}

    /**
     * Returns the Expression that is to be displayed
     * @return the Expression to be displayed
     */
    public ExpressionNode getExpNode(){return this.expNode;}

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Write\n";
        answer += this.expNode.indentedToString(level +1);
        return answer;
    }
}
