
package syntaxtree;

/**
 * Represents a Pascal Program
 * @author Erik Steinmetz
 */
public class ProgramNode extends SyntaxTreeNode {
    
    
    private String name;                            //The name of the program
    private DeclarationsNode variables;             //Holds all the variables
    private SubProgramDeclarationsNode functions;   //Holds all the functions
    private CompoundStatementNode main;             //Holds the main Statements

    /**
     * Constructor that takes in the name
     * @param aName The name of the subprogram
     */
    public ProgramNode( String aName) {
        this.name = aName;
    }

    /**
     * Returns the DeclarationsNode
     * @return the subprograms DeclarationsNode
     */
    public DeclarationsNode getVariables() {
        return variables;
    }

    /**
     * Sets the Variables to a Declarations node
      * @param variables to set as the Declarations
     */
    public void setVariables(DeclarationsNode variables) {
        this.variables = variables;
    }

    /**
     * Returns the functions
     * @return the functions of the subprogram
     */
    public SubProgramDeclarationsNode getFunctions() {
        return functions;
    }

    /**
     * Sets the Functions
     * @param functions as the SubprogramDeclarationsNode
     */
    public void setFunctions(SubProgramDeclarationsNode functions) {
        this.functions = functions;
    }

    /**
     * Returns the Statements
     * @return the statements from the function
     */
    public CompoundStatementNode getMain() {
        return main;
    }

    /**
     * Sets the main Statements
     * @param main to be set as the main statement
     */
    public void setMain(CompoundStatementNode main) {
        this.main = main;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Program: " + name + "\n";
        answer += variables.indentedToString( level + 1);
        answer += functions.indentedToString( level + 1);
        answer += main.indentedToString( level + 1);
        return answer;
    }
}
