package syntaxtree;

import scanner.TokenType;

/**
 * Represents a value or number in an expression.
 * @author Erik Steinmetz
 */
public class ValueNode extends ExpressionNode {
    /** The attribute associated with this node. */
    private String attribute;
    
    /**
     * Creates a ValueNode with the given attribute.
     * checks to see if the value is an integer or real
     * and assigns accordingly
     * @param attr The attribute for this value node.
     */
    public ValueNode( String attr) {
        super(null);
        this.attribute = attr;
        if(attr.contains("."))
            super.dataType=TokenType.REAL;
        else
            super.dataType=TokenType.INTEGER;
    }
    
    /** 
     * Returns the attribute of this node.
     * @return The attribute of this ValueNode.
     */
    public String getAttribute() { return( this.attribute);}
    
    /**
     * Returns the attribute as the description of this node.
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return( attribute);
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Value: " + this.attribute + " (" + this.dataType +")"+"\n";
        return answer;
    }

    /**
     * Compares two Value to check if they are equal.
     * @param o to be compared
     * @return True if they equal. false otherwise
     */
    @Override
    public boolean equals( Object o) {
        boolean answer = false;
        if( o instanceof ValueNode) {
            ValueNode other = (ValueNode)o;
            if( (this.attribute.equals( other.attribute))&& (other.getDataType() == super.dataType)) answer = true;
        }
        return answer;
    }    
}
