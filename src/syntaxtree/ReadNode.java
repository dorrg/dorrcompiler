package syntaxtree;

/**
 * ReadNode holds the information need to read in from the terminal in MIPS assembly
 * When it takes in an an argument, it will store it to the variable
 * @author Greg
 * created on 3/24/2017.
 */
public class ReadNode extends StatementNode {
    //could be function or array or procedure

    private VariableNode var; //Stores input information to this variable

    /**
     * Constructor that accepts one argument
     * @param var to store the number taken in
     */
    public ReadNode(VariableNode var){
        this.var = var;
    }

    /**
     * returns the variable
     * @return the VariableNode
     */
    public VariableNode getVar() {
        return this.var;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Read\n";
        answer += var.indentedToString(level +1);
        return answer;
    }
}
